package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Paths;

public class MainFX extends Application {

    Button button;

    @Override
    public void start(Stage stage) throws IOException {

        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        Label l = new Label("Hello, JavaFX ");// + javafxVersion + ", running on Java " + javaVersion + ".");

        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/LoginView/loginView.fxml"));

        Media nebunie = new Media(Paths.get(".\\src\\main\\resources\\LoginView\\little_dark_age.mp3").toUri().toString());

        MediaPlayer mediaPlayer = new MediaPlayer(nebunie);

        //mediaPlayer.setAutoPlay(true);

        //Media media = new Media("http://path/file_name.mp3");

        // Media media = new Media(new File("Love_Light_Club-Dreamer.mp3").toURI().toString());
        try {
            //AnchorPane root = loader.load();
            StackPane layout = loader.load();

            //button = new Button();

           // button.setText("Hy!");
            //layout.getChildren().add(button);





            Scene scene = new Scene(layout, 640, 480);
            stage.setScene(scene);
            stage.getIcons().add(new Image(".\\LoginView\\icon.jpg"));
            stage.show();
        }
        catch (IOException exception)
        {
            System.out.println(exception.getCause() + " " + exception.getMessage());
        }
    }



    public static void main(String[] args) {
        launch();
    }

}