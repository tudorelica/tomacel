package socialnetwork.service;

import socialnetwork.domain.CustomEvent;
import socialnetwork.repository.RepositoryDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EvenimenteServiceDb {
    RepositoryDb <Long, CustomEvent> repo;

    public EvenimenteServiceDb(RepositoryDb <Long, CustomEvent> repo)
    {
        this.repo = repo;
    }

    public CustomEvent addEvent(CustomEvent newE){
        Optional<CustomEvent> task = repo.save(newE);

        if(task.isPresent())
            return task.get();
        else
            return null;
    }

    public CustomEvent delEvent(Long id){
        Optional<CustomEvent> task = repo.delete(id);

        if(task.isPresent())
            return task.get();
        else
            return null;
    }

    public String addParticipant(Long idEvent, Long idUser){
        Optional<CustomEvent> task = repo.findOne(idEvent);

        if(task.isPresent()) {
            CustomEvent event = task.get();
            event.addId(idUser);
            repo.update(event);

            return event.getStringIds();
        }
        else
            return null;
    }

    public String delParticipant(Long idEvent, Long idUser){
        Optional<CustomEvent> task = repo.findOne(idEvent);

        if(task.isPresent()) {
            CustomEvent event = task.get();
            event.removeId(idUser);
            repo.update(event);

            return event.getStringIds();
        }
        else
            return null;
    }

    public Iterable<CustomEvent> getAllEvents(){
        return repo.findAll();
    }

    public CustomEvent getOneEvent(Long idEvent){
        Optional<CustomEvent> task = repo.findOne(idEvent);

        if(task.isPresent()) {
            return task.get();
        }
        else
        {
            return null;
        }
    }

    public Iterable<CustomEvent> getEventsByUserId(Long idUser){
        List <CustomEvent> rez = new ArrayList<CustomEvent>();

        for (CustomEvent event : getAllEvents()) {
            for(Long ids : event.getIds())
                if (ids == idUser)
                {
                    rez.add(event);
                    break;
                }
        }

        return rez;
    }

    public Iterable<CustomEvent> getEventsCreatedBy(Long idUser){
        List <CustomEvent> rez = new ArrayList<CustomEvent>();

        for (CustomEvent event : getAllEvents()) {
            if (event.getCreatorId() == idUser)
            {
                rez.add(event);
            }
        }

        return rez;
    }

    public Iterable<CustomEvent> getEligibleEventsFromFriends(Iterable<Long> friendIDs){
        List <CustomEvent> rez = new ArrayList<CustomEvent>();

        for (Long friend : friendIDs)
            for (CustomEvent event : getEventsCreatedBy(friend))
            {
                rez.add(event);
            }

        return rez;
    }
}
