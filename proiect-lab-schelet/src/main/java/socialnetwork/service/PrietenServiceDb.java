package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.RepositoryDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PrietenServiceDb {
    private RepositoryDb<Tuple<Long,Long>, Prietenie> repo;

    public PrietenServiceDb(RepositoryDb<Tuple<Long,Long>, Prietenie> repo) {
        this.repo = repo;
    }

    public Prietenie addPrietenie(Prietenie messageTask) {
        int nr = 0;
        if (repo.findOne(messageTask.getId()).isPresent())
            nr++;

        if (repo.findOne(new Tuple<Long, Long>(messageTask.getId().getRight(),messageTask.getId().getLeft())).isPresent())
            nr++;

        if (nr == 0) {
            Optional<Prietenie> task = repo.save(messageTask);

            if (task.isPresent()) {
                Prietenie p = task.get();
                return p;
            } else
                return null;
        }
        else
            return null;
    }

    public Prietenie delPrietenie(Tuple<Long, Long> id)
    {
        Optional<Prietenie> task = repo.delete(id);

        if(task.isPresent())
        {
            Prietenie p = task.get();
            return p;
        }
        else
            return null;
    }

    public List<Long> getSpecificFriends(Long id) {
        List<Long> friends = new ArrayList<Long>();

        for (Prietenie friendship : repo.findAll()) {
            if (((friendship.getId().getRight() == id)
                    || (friendship.getId().getLeft() == id))
                    && (friendship.getStatus() == 0))
            {
                if (friendship.getId().getRight() == id)
                    friends.add(friendship.getId().getLeft());
                else
                    friends.add(friendship.getId().getRight());
            }
        }
        return friends;
    }

    public List<Prietenie> getSpecificFriendsObjects(Long id) {
        List<Prietenie> friends = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll()) {
            if (((friendship.getId().getRight() == id)
                    || (friendship.getId().getLeft() == id))
                    && (friendship.getStatus() == 0))
            {
                friends.add(friendship);
            }
        }
        return friends;
    }

    public void sendFriendRequest(Long idSender, Long idReceiver)
    {
        Prietenie newFriendship = new Prietenie(-1);

        newFriendship.setId(new Tuple<Long, Long>(idSender, idReceiver));
        repo.save(newFriendship);
    }

    public void updateFriendRequest(Long idSender, Long idReceiver, int status)
    {
        Prietenie updatedFriendship = repo.delete(new Tuple<Long, Long>(idSender, idReceiver)).get();

        updatedFriendship.setStatus(status);
        repo.save(updatedFriendship);
    }

    public List<Prietenie> specificFriendRequests(Long id)
    {
        List<Prietenie> friendRequests = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll())
            if ((friendship.getStatus() == -1) && (friendship.getId().getRight() == id))
                friendRequests.add(friendship);

        return friendRequests;
    }

    public List<Prietenie> specificFriendPendings(Long id)
    {
        List<Prietenie> friendPendings = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll())
            if ((friendship.getStatus() == -1) && (friendship.getId().getLeft() == id))
                friendPendings.add(friendship);

        return friendPendings;
    }

    public Iterable<Prietenie> getAll(){
        return repo.findAll();
    }
}
