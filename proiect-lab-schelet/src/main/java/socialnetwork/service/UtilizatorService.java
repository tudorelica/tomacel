package socialnetwork.service;

import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class UtilizatorService  {
    private Repository<Long, Utilizator> repo;

    public UtilizatorService(Repository<Long, Utilizator> repo) {
        this.repo = repo;
    }

    public Utilizator addUtilizator(Utilizator messageTask) {
        Utilizator task = repo.save(messageTask);
        return task;
    }

    public Utilizator delUtilizator(Long id) {
        Utilizator task = repo.delete(id);
        return task;
    }

    public boolean idCheck(long id)
    {
        if (repo.findOne(id) == null)
            return false;
        return true;
    }

    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }

    /**
     @return id(Long) of a user based on his first and second names
     @since LAB4 | Week 8 | 16.11.2020
     */
    public Long getId(String s, String s1) {
        Long id = -1L;

        for (Utilizator user : repo.findAll()) {
            if (user.getFirstName().equals(s1) && user.getLastName().equals(s))
                id = user.getId();
        }

        return id;
    }

    /**
     @return a users data based on his specified id
     @since LAB4 | Week 8 | 17.11.2020
     */
    public Utilizator getById(Long id)
    {
        return repo.findOne(id);
    }

    public String getUserById(Long id)
    {
        return repo.findOne(id).getUsername();
    }

    public Utilizator accountCheck(String username, String password) {
        for ( Utilizator user : repo.findAll())
        {
            System.out.println("1: " + user.getUsername() + "; " + user.getPassword() + "  =?  2: "+ username + "; " + password);
            if ((user.getUsername().matches(username)) && (user.getPassword().matches(password)))
                return user;
        }
        return null;
    }

    ///TO DO: add other methods
}
