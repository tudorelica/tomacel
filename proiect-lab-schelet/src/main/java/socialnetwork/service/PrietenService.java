package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class PrietenService {
    private Repository<Tuple<Long, Long>, Prietenie> repo;

    public PrietenService(Repository<Tuple<Long, Long>, Prietenie> repo) {
        this.repo = repo;
    }

    public Prietenie addPrietenie(Prietenie messageTask) {
        Prietenie task = repo.save(messageTask);
        return task;
    }

    public Prietenie delPrietenie(Tuple<Long, Long> id)
    {
        Prietenie task = repo.delete(id);
        return task;
    }

    public Iterable<Prietenie> getAll() {
        return repo.findAll();
    }

/**
    @return a list of id's(Long) of users that are friends with the user which has the specified id
    @since LAB4 | Saptamana 8 | 17.11.2020
 */
    public List<Long> getSpecificFriends(Long id) {
        List<Long> friends = new ArrayList<Long>();

        for (Prietenie friendship : repo.findAll()) {
            if (((friendship.getId().getRight() == id)
                    || (friendship.getId().getLeft() == id))
                    && (friendship.getStatus() == 0))
            {
                if (friendship.getId().getRight() == id)
                    friends.add(friendship.getId().getLeft());
                else
                    friends.add(friendship.getId().getRight());
            }
        }
        return friends;
    }

    public List<Prietenie> getSpecificFriendsObjects(Long id) {
        List<Prietenie> friends = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll()) {
            if (((friendship.getId().getRight() == id)
                    || (friendship.getId().getLeft() == id))
                    && (friendship.getStatus() == 0))
            {
                friends.add(friendship);
            }
        }
        return friends;
    }

    public void sendFriendRequest(Long idSender, Long idReceiver)
    {
        Prietenie newFriendship = new Prietenie(-1);

        newFriendship.setId(new Tuple<Long, Long>(idSender, idReceiver));
        repo.save(newFriendship);
    }

    public void updateFriendRequest(Long idSender, Long idReceiver, int status)
    {
        Prietenie updatedFriendship = repo.delete(new Tuple<Long, Long>(idSender, idReceiver));

        updatedFriendship.setStatus(status);
        repo.save(updatedFriendship);
    }

    public List<Prietenie> specificFriendRequests(Long id)
    {
        List<Prietenie> friendRequests = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll())
            if ((friendship.getStatus() == -1) && (friendship.getId().getRight() == id))
                friendRequests.add(friendship);

        return friendRequests;
    }

    public List<Prietenie> specificFriendPendings(Long id)
    {
        List<Prietenie> friendPendings = new ArrayList<Prietenie>();

        for (Prietenie friendship : repo.findAll())
            if ((friendship.getStatus() == -1) && (friendship.getId().getLeft() == id))
                friendPendings.add(friendship);

        return friendPendings;
    }
}
