package socialnetwork.service;

import socialnetwork.domain.CererePrietenieDTO;
import socialnetwork.domain.Mesaj;
import socialnetwork.repository.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class MesajService  {
    private Repository<Long, Mesaj> repo;

    public MesajService(Repository<Long, Mesaj> repo) {
        this.repo = repo;
    }

    public Mesaj addUMesaj(Mesaj messageTask) {
        Mesaj task = repo.save(messageTask);
        return task;
    }

    public Mesaj delMesaj(Long id) {
        Mesaj task = repo.delete(id);
        return task;
    }

    public boolean idCheck(long id)
    {
        if (repo.findOne(id) == null)
            return false;
        return true;
    }

    public Iterable<Mesaj> getAll(){
        return repo.findAll();
    }

    public Vector<Mesaj> sentMessages(Long senderId)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();

        for (Mesaj msj : getAll())
            if (msj.getSenderId() == senderId)
                rez.add(msj);

        return rez;
    }

    public Vector<Mesaj> receivedMessages(Long senderId)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();

        for (Mesaj msj : getAll())
            for (Long receiverId : msj.getReceiversId())
                if (receiverId == senderId)
                    rez.add(msj);

        return rez;
    }

    public List<CererePrietenieDTO> conversationGetter(Long id1, Long id2)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();
        Vector<CererePrietenieDTO> rez2 = new Vector<CererePrietenieDTO>();
        rez2.add(new CererePrietenieDTO("", 1L, "", ""));

        for (Mesaj msj : getAll()) {
            //System.out.println(msj.toString() + "\n\n");
            boolean bun = true;



            if (msj.getReceiversId().size() > 1){
                bun = false;
                //System.out.println("too many recievers: " + id1 + ";" + id2 + ";" + msj.getReceiversId().size());

            }
            else
            {
                if ((msj.getSenderId() != id1) && (msj.getSenderId() != id2)) {
                    bun = false;
                    // System.out.println("sender unruly: " + id1 + ";" + id2);

                }
                else
                {
                    for (Long receiverId : msj.getReceiversId()) {
                        System.out.println("recvers:" + receiverId);
                        if ((receiverId != id1) && (receiverId != id2)) {
                            bun = false;
                           // System.out.println("bad reciever" + " " + receiverId);
                        }
                    }
                }
            }



            if (bun) {
                System.out.println("message added!");
                rez.add(msj);
                //if (msj.getSenderId() == id1)
                    //rez.add(new CererePrietenieDTO("", msj.getSenderId(), msj.getMessage(), ""));
                //else
                    //rez.add(new CererePrietenieDTO(msj.getMessage(), msj.getSenderId(), "", ""));
            }
        }

        rez.sort(new Comparator<Mesaj>() {
                     public int compare(Mesaj m1, Mesaj m2) { return m1.getDate().compareTo(m2.getDate()); }}
                     );

        for (Mesaj msg : rez)
        {
            System.out.println(msg);
            if (msg.getSenderId() == id1)
                rez2.add(new CererePrietenieDTO("", msg.getSenderId(), msg.getMessage() + "    ", ""));
            else
                rez2.add(new CererePrietenieDTO("    " + msg.getMessage(), msg.getSenderId(), "", ""));
        }

        return rez2.stream().collect(Collectors.toList());
    }


    /**
     @return a users data based on his specified id
     @since LAB4 | Week 8 | 17.11.2020
     */
    public Mesaj getById(Long id)
    {
        return repo.findOne(id);
    }



}
