package socialnetwork.service;

import socialnetwork.domain.Utilizator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;

import java.util.Optional;

public class UtilizatorServiceDb {
    private PagingRepository<Long, Utilizator> repo;

    public UtilizatorServiceDb(PagingRepository<Long, Utilizator> repo) {
        this.repo = repo;
    }

    /**Tested*/
    public Utilizator addUtilizator(Utilizator messageTask) {
        Optional<Utilizator> task = repo.save(messageTask);

        if(task.isPresent())
        {
            Utilizator util = task.get();
            return util;
        }
        else
            return null;
    }

    /**Tested*/
    public Utilizator delUtilizator(Long id) {
        Optional<Utilizator> task = repo.delete(id);

        if(task.isPresent())
        {
            Utilizator util = task.get();
            return util;
        }
        else
            return null;
    }

    /**Tested*/
    public boolean idCheck(long id)
    {
        if (repo.findOne(id).isPresent())
            return true;
        return false;
    }

    /**Tested*/
    public Long getId(String s, String s1) {
        Long id = -1L;

        for (Utilizator user : repo.findAll()) {
            if (user.getFirstName().equals(s1) && user.getLastName().equals(s))
                id = user.getId();
        }

        return id;
    }

    /**Tested*/
    public Utilizator getById(Long id)
    {

        if (repo.findOne(id).isPresent()) {
            Utilizator util = repo.findOne(id).get();
            return util;
        }
        else
            return null;
    }

    /**Tested*/
    public String getUserById(Long id)
    {
        if(repo.findOne(id).isPresent())
            return repo.findOne(id).get().getUsername();
        else
            return null;
    }

    public Page<Utilizator> getPagedUsers(int page) {
        Pageable p = new PageableImplementation(page,5);
        return repo.findAll(p);
    }

    /**Tested*/
    public Utilizator accountCheck(String username, String password) {
        for ( Utilizator user : repo.findAll())
        {
            System.out.println("1: " + user.getUsername() + "; " + user.getPassword() + "  =?  2: "+ username + "; " + password);
            if ((user.getUsername().matches(username)) && (user.getPassword().matches(password)))
                return user;
        }
        return null;
    }

    /**Tested*/
    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }

}
