package socialnetwork.service;

import socialnetwork.domain.CererePrietenieDTO;
import socialnetwork.domain.Mesaj;
import socialnetwork.repository.RepositoryDb;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;

public class MesajServiceDb {
    private RepositoryDb< Long, Mesaj> repo;

    public MesajServiceDb(RepositoryDb< Long, Mesaj> repo) {
        this.repo = repo;
    }

    public Mesaj addUMesaj(Mesaj messageTask) {
        Optional<Mesaj> task = repo.save(messageTask);

        if(task.isPresent())
        {
            Mesaj m = task.get();
            return m;
        }
        else
            return null;
    }

    public Mesaj delMesaj(Long id) {
        Optional<Mesaj> task = repo.delete(id);

        if(task.isPresent())
        {
            Mesaj m = task.get();
            return m;
        }
        else
            return null;
    }

    public boolean idCheck(long id)
    {
        if (repo.findOne(id).isPresent())
            return true;
        return false;
    }

    public Iterable<Mesaj> getAll(){
        return repo.findAll();
    }

    public Vector<Mesaj> sentMessages(Long senderId)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();

        for (Mesaj msj : getAll())
            if (msj.getSenderId() == senderId)
                rez.add(msj);

        return rez;
    }

    public Vector<Mesaj> receivedMessages(Long senderId)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();

        for (Mesaj msj : getAll())
            for (Long receiverId : msj.getReceiversId())
                if (receiverId == senderId)
                    rez.add(msj);

        return rez;
    }

    public List<CererePrietenieDTO> conversationGetter(Long id1, Long id2)
    {
        Vector<Mesaj> rez = new Vector<Mesaj>();
        Vector<CererePrietenieDTO> rez2 = new Vector<CererePrietenieDTO>();
        rez2.add(new CererePrietenieDTO("", 1L, "", ""));

        for (Mesaj msj : getAll()) {
            //System.out.println(msj.toString() + "\n\n");
            boolean bun = true;



            if (msj.getReceiversId().size() > 1){
                bun = false;
                //System.out.println("too many recievers: " + id1 + ";" + id2 + ";" + msj.getReceiversId().size());

            }
            else
            {
                if ((msj.getSenderId() != id1) && (msj.getSenderId() != id2)) {
                    bun = false;
                    // System.out.println("sender unruly: " + id1 + ";" + id2);

                }
                else
                {
                    for (Long receiverId : msj.getReceiversId()) {
                        System.out.println("recvers:" + receiverId);
                        if ((receiverId != id1) && (receiverId != id2)) {
                            bun = false;
                            // System.out.println("bad reciever" + " " + receiverId);
                        }
                    }
                }
            }



            if (bun) {
                System.out.println("message added!");
                rez.add(msj);
                //if (msj.getSenderId() == id1)
                //rez.add(new CererePrietenieDTO("", msj.getSenderId(), msj.getMessage(), ""));
                //else
                //rez.add(new CererePrietenieDTO(msj.getMessage(), msj.getSenderId(), "", ""));
            }
        }

        rez.sort(new Comparator<Mesaj>() {
            public int compare(Mesaj m1, Mesaj m2) { return m1.getDate().compareTo(m2.getDate()); }}
        );

        for (Mesaj msg : rez)
        {
            System.out.println(msg);
            if (msg.getSenderId() == id1)
                rez2.add(new CererePrietenieDTO("", msg.getSenderId(), msg.getMessage() + "    ", ""));
            else
                rez2.add(new CererePrietenieDTO("    " + msg.getMessage(), msg.getSenderId(), "", ""));
        }

        return rez2.stream().collect(Collectors.toList());
    }

    public Mesaj getById(Long id)
    {
        Optional<Mesaj> task =  repo.findOne(id);

        if(task.isPresent())
        {
            Mesaj m = task.get();
            return m;
        }
        else
            return null;
    }


}
