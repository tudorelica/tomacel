package socialnetwork.ui;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.RepositoryException;
import socialnetwork.repository.file.PrietenFile;
import socialnetwork.service.PrietenService;
import socialnetwork.service.UtilizatorService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import socialnetwork.domain.Tuple;

public class Ui {
    private UtilizatorService service_universal_user;
    private PrietenService service_universal_friend;


    public Ui(UtilizatorService service, PrietenService service2) {
        this.service_universal_user = service;
        this.service_universal_friend = service2;
    }

    public void Run() {
        String input = "";

        menuGenerator(input);
    }

    private void menuGenerator(String input) {
        while(1 == 1)
        {
            printMenu();
            printConsoleMarker();
            input = readInput();
            operationFork(input);
        }
    }

    private void printMenu()
    {
        System.out.println("1 -> Adaugati utilizator");
        System.out.println("2 -> Stergeti utilizator");
        System.out.println("3 -> Afisati utilizatorii");
        System.out.println("4 -> Adaugati relatie de prietenie");
        System.out.println("5 -> Stergeti relatie de prietenie");
        System.out.println("6 -> Afisati relatii de prietenie");
        System.out.println("7 -> Afisati prietenii unui anumit utilizator");
        System.out.println("q -> Inchideti programul");
    }

    private void printConsoleMarker()
    {
        System.out.print(">>>");
    }

    private String readInput() {
        try {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));

            return reader.readLine();
        }
        catch (IOException ioException)
        {
            System.out.println("Eroare la citirea datelor");
            return null;
        }
    }

    private void operationFork(String in) {
        switch (in) {
            case "1":
                //Adaugare
                uiAdd();
                break;
            case "2":
                //Stergere
                uiDel();
                break;
            case "3":
                //Afisare
                uiShow();
                break;
            case "4":
                //Afisare
                uiAddFriend();
                break;
            case "5":
                //Afisare
                uiDelFriend();
                break;
            case "6":
                //Afisare
                uiShowFriends();
                break;
            case "7":
                //Afisare
                uiShowSpecificFriends();
                break;
            case "q":
                uiQ();
                break;
            default:
                System.out.println("Operatie necunoscuta:  " + in);
        }

    }
    /** uiAddFriend, uiDelFriend, uiShowFriends, uiShowSpecificFriends*/

    private void uiShowSpecificFriends() {

        System.out.println("Introduceti numele si prenumele utilizator separate prin ';'");
        printConsoleMarker();
        String inAdd = readInput();

        List<String> attr= Arrays.asList(inAdd.split(";"));


        System.out.println("Prietenii lui " + attr.get(0) + " " + attr.get(1) + ":");

        Utilizator user = service_universal_user.getById(service_universal_user.getId(attr.get(0), attr.get(1)));


        List<Long> friendIds = service_universal_friend
                                    .getSpecificFriends(service_universal_user.getId(attr.get(0), attr.get(1)));
        friendIds.stream()
                .forEach(x -> System.out.println(service_universal_user.getById(x).getFirstName() +
                        ";" + service_universal_user.getById(x).getLastName()));


        /*
        List<String> friendsMap =
                StreamSupport.stream(service_universal_friend.getAll().spliterator(), false)
                        .filter(x->(x.getId().getRight() == user.getId() || x.getId().getLeft() == user.getId()))
                        .map(x->{if (x.getId().getRight() == user.getId())
                                    return new String("" + service_universal_user.getById(x.getId().getLeft()));});


         StreamSupport.stream(service_universal_friend.getAll().spliterator(), false)
                        .filter(x->(x.getId().getRight() == user.getId() || x.getId().getLeft() == user.getId()))
                        .map(x->{if (x.getId().getRight() == user.getId())
                                    return new String("" + service_universal_user.getById(x.getId().getLeft())
                                            .getFirstName() +
                                            ";" + service_universal_user.getById(x.getId().getLeft())
                                            .getLastName()
                                            + ": (" + x.getDate().toString() + ")");
                                else
                                    return new String("" + service_universal_user.getById(x.getId().getRight())
                                            .getFirstName() +
                                            ";" + service_universal_user.getById(x.getId().getRight())
                                            .getLastName()
                                            + ": (" + x.getDate().toString() + ")");
                                })
                        .forEach(System.out::println);
         */

    }

   /*
        Map<Utilizator, LocalDate> friends = StreamSupport.stream(spliterator, false)
                .filter(x->(x.getId().getRight()==u.getId() || x.getId().getLeft()==u.getId()))
                .map(x->{if(x.getId().getLeft()==u.getId()) return new Tuple<Long, LocalDate>(x.getId().getRight(),x.getDate().toLocalDate());
                else if(x.getId().getRight()==u.getId()) return new Tuple<Long, LocalDate>(x.getId().getLeft(), x.getDate().toLocalDate());
                    return null;
                })
                .collect(Collectors.toMap(x->repoUsers.findOne((x.getLeft())).get(), x->x.getRight()));
        return friends; */

    //service_universal_friend.getSpecificFriends(10L/*service_universal_user.getId(attr.get(0), attr.get(1))*/).forEach(System.out::println);
    /**service_universal_friend.getSpecificFriends(service_universal_user.getId(attr.get(0), attr.get(1)))
     .forEach(friendship -> System.out.println(service_universal_user.getById(friendship).getFirstName() +
     ";" + service_universal_user.getById(friendship).getLastName() ));
     */
    private void uiAddFriend()  {
        System.out.println("Introduceti id-urile prietenilor separate prin ';'");
        printConsoleMarker();

        String inAdd = readInput();

        List<String> attr= Arrays.asList(inAdd.split(";"));
        Prietenie friendship = new Prietenie();
        friendship.setId(new Tuple(Long.parseLong(attr.get(0)), Long.parseLong(attr.get(1))));

        boolean idCheck1 = false;
        boolean idCheck2 = false;

        for (Utilizator elem : service_universal_user.getAll())
        {
            if (elem.getId() == Long.parseLong(attr.get(0)))
                idCheck1 = true;
            if (elem.getId() == Long.parseLong(attr.get(1)))
                idCheck2 = true;
        }
        if (idCheck1 && idCheck2) {
            service_universal_friend.addPrietenie(friendship);
            System.out.println("Prietenie adaugata: " + friendship.toString());
        }
        else
            System.out.println("Id-urile prietenilor nu sunt valide!");
    }

    private void uiDelFriend() {
        System.out.println("Introduceti id-ul utilizatorilor a caror prietenie doriti sa o eliminati separate prin ';'");
        printConsoleMarker();

        String inAdd = readInput();
        List<String> attr= Arrays.asList(inAdd.split(";"));
        service_universal_friend.delPrietenie(new Tuple(Long.parseLong(attr.get(0)), Long.parseLong(attr.get(1))));

        System.out.println("User Relatie de prietenie stearsa: " + inAdd );
    }

    private void uiShowFriends()
    {
        System.out.println("Relatii de prietenie:");

        service_universal_friend.getAll().forEach(System.out::println);
    }
    /** uiAdd, uiDel, uiShow, uiQ*/

    private void uiQ()
    {
        System.out.println("Closing..");
        System.exit(0);
    }

    private void uiAdd() {
        System.out.println("Introduceti id-ul, numele, prenumele, username-ul si parola noului utilizator separate prin ';'");
        printConsoleMarker();
        String inAdd = readInput();

        List<String> attr= Arrays.asList(inAdd.split(";"));
        Utilizator user = new Utilizator(attr.get(1),attr.get(2),attr.get(3),attr.get(4));

        user.setId(Long.parseLong(attr.get(0)));


        if (service_universal_user.addUtilizator(user) == null)
            System.out.println("User-ul nu a fost adaugat.");
        else
            System.out.println("User adaugat: " + user.toString() );
    }

    private void uiDel() {
        System.out.println("Introduceti id-ul utilizator pe care doriti sa il stergeti:");
        printConsoleMarker();

        String inAdd = readInput();
        Long userId = Long.parseLong(inAdd);
        Utilizator user = service_universal_user.delUtilizator(userId);

        Vector<Tuple<Long, Long>> idVec = new Vector<>();


        for (Prietenie elem : service_universal_friend.getAll())
        {
            if ((elem.getId().getRight() == userId) || (elem.getId().getLeft() == userId))
                idVec.add(elem.getId());
        }

        for (Tuple<Long, Long> id : idVec)
            service_universal_friend.delPrietenie(id);

        System.out.println("User sters: " + user.toString() );
    }

    private void uiShow() {
        System.out.println("Utilizatori:");

        service_universal_user.getAll().forEach(System.out::println);
    }
}
