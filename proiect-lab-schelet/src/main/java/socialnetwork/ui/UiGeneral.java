package socialnetwork.ui;


import socialnetwork.domain.*;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.repository.file.UtilizatorFile;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.PrietenFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.MesajService;
import socialnetwork.service.PrietenService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.Ui;
import sun.nio.ch.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class UiGeneral {

    public UiGeneral() {
    }

    public void Run() {
        String input = "";

        menuGenerator(input);
    }

    private void menuGenerator(String input) {
        while(1 == 1)
        {
            printMenu();
            printConsoleMarker();
            input = readInput();
            operationFork(input);
        }
    }


    private void printMenu()
    {
        System.out.println("1 -> Admin Ui");
        System.out.println("2 -> User Ui");
        System.out.println("q -> Inchideti programul");
    }

    private void printConsoleMarker()
    {
        System.out.print(">>>");
    }

    private String readInput() {
        try {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));

            return reader.readLine();
        }
        catch (IOException ioException)
        {
            System.out.println("Eroare la citirea datelor");
            return null;
        }
    }

    private void operationFork(String in) {
        switch (in) {
            case "1":
                //Adaugare
                uiAdmin();
                break;
            case "2":
                //Stergere
                uiUser();
                break;
            case "q":
                uiQ();
                break;
            default:
                System.out.println("Operatie necunoscuta:  " + in);
        }

    }

    private void uiAdmin()
    {
        System.out.println("<Admin Ui>");
        //String fileName=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        System.out.println("Hey!");
        //String fileName="D:\\InformaticaRomana\\AN II\\Metode Avansate de Programare\\lab6\\proiect-lab-schelet\\proiect-lab-schelet\\data\\users.csv";
        String fileName=".\\data\\users.csv";
        String fileName2=".\\data\\friends.csv";


        Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileName
                , new UtilizatorValidator());


        Repository<Tuple<Long, Long>, Prietenie> prietenFileRepository = new PrietenFile(fileName2,
                new Validator<Prietenie>() {
                    @Override
                    public void validate(Prietenie entity) throws ValidationException {

                    }
                });
        //Utilizator test = new Utilizator("Tudor", "Coman");
        //test.setId((long) 5);

        //userFileRepository.save(test);
        //userFileRepository.delete((long) 2);

        userFileRepository.findAll().forEach(System.out::println);
        prietenFileRepository.findAll().forEach(System.out::println);

        UtilizatorService service = new UtilizatorService(userFileRepository);
        PrietenService service2 = new PrietenService(prietenFileRepository);
        Ui test_ui = new Ui(service, service2);

        test_ui.Run();
    }

    private void uiUser()
    {
        System.out.println("<User Ui>");

        System.out.println("Hey!");

        String fileName=".\\data\\users.csv";
        String fileName2=".\\data\\friends.csv";
        String fileName3=".\\data\\messages.csv";


        Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileName
                , new UtilizatorValidator());


        Repository<Tuple<Long, Long>, Prietenie> prietenFileRepository = new PrietenFile(fileName2,
                new Validator<Prietenie>() {
                    @Override
                    public void validate(Prietenie entity) throws ValidationException {

                    }
                });

        Repository<Long, Mesaj> mesajFileRepository = new MesajFile(fileName3, new Validator<Mesaj>() {
            @Override
            public void validate(Mesaj entity) throws ValidationException {

            }
        });



        userFileRepository.findAll().forEach(System.out::println);
        prietenFileRepository.findAll().forEach(System.out::println);

        UtilizatorService service = new UtilizatorService(userFileRepository);
        PrietenService service2 = new PrietenService(prietenFileRepository);
        MesajService service3 = new MesajService(mesajFileRepository);

        System.out.println("Username: ");
        String username = readInput();

        System.out.println("Password: ");
        String password = readInput();


        try {
            UiUser test_ui = new UiUser(service, service2, service3, username, password);
            test_ui.Run();
        }
        catch (Exception ex)
        {
            System.out.println("Nume de utilizator sau parola incorecta");
        }


    }

    private void uiQ()
    {
        System.out.println("Closing..");
        System.exit(0);
    }

}
