package socialnetwork.ui;


import jdk.swing.interop.SwingInterOpUtils;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.RepositoryException;
import socialnetwork.repository.file.PrietenFile;
import socialnetwork.service.MesajService;
import socialnetwork.service.PrietenService;
import socialnetwork.service.UtilizatorService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Array;
import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import socialnetwork.domain.Tuple;

public class UiUser {
    private UtilizatorService service_universal_user;
    private PrietenService service_universal_friend;
    private Utilizator user;
    private MesajService service_mesaj;
    //private List<Utilizator> friends;


    public UiUser(UtilizatorService service, PrietenService service2,MesajService service3, String username, String password) throws Exception {
        this.service_universal_user = service;
        this.service_universal_friend = service2;
        this.service_mesaj = service3;
        this.user = service_universal_user.accountCheck(username, password);
        if (this.user == null)
            throw new Exception("Unknown user account!");
    }

    private void validate(Utilizator user){
        System.out.println("Validating user..");;
    }

    public void Run() {
        String input = "";

        menuGenerator(input);
    }

    private void menuGenerator(String input) {
        while(1 == 1)
        {
            printMenu();
            printConsoleMarker();
            input = readInput();
            operationFork(input);
        }
    }

    private void printMenu()
    {
        uiShow();
        System.out.println("1 -> Mesaje primite");
        System.out.println("2 -> Cereri de prietenie");
        System.out.println("3 -> Trimite un mesaj");
        System.out.println("4 -> Trimite o cerere de prietenie");
        System.out.println("5 -> Afiseaza lista de prieteni");
        System.out.println("6 -> Afiseaza mesajele trimise");
        System.out.println("7 -> Afiseaza conversatie");
        System.out.println("q -> Inchide programul");

    }

    private void printConsoleMarker()
    {
        System.out.print(">>>");
    }

    private String readInput() {
        try {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));

            return reader.readLine();
        }
        catch (IOException ioException)
        {
            System.out.println("Eroare la citirea datelor");
            return null;
        }
    }

    private void operationFork(String in) {
        uiShow();

        switch (in) {
            case "1":
                //Afisare mesaje primite
                uiMessageShow();
                break;
            case "2":
                //Afisare cereri de prietenie
                uiRequestShow();
                break;
            case "3":
                //Trimitere mesaj
                uiMessage();
                break;
            case "4":
                //Trimitere cerere de prietenie
                uiAddFriend();
                break;
            case "5":
                //Afisare lista de prieteni
                uiShowSpecificFriends();
                break;
            case "6":
                //Afisare mesaje trimise
                uiShowSentMessages();
                break;
            case "7":
                //Afisare conversatie
                uiShowConversation();
                break;
            case "q":
                uiQ();
                break;
            default:
                System.out.println("Operatie necunoscuta:  " + in);
        }

    }

    private void uiShowConversation() {
        System.out.println("Introduceti ID-ul unui alt participant la conversatie: ");
        printConsoleMarker();
        Long otherId = Long.parseLong(readInput());

        List<Mesaj> list1 = service_mesaj.sentMessages(this.user.getId());
        List<Mesaj> list2 = service_mesaj.receivedMessages(otherId);
        Vector<Mesaj> v1 = new Vector<Mesaj>();
        for (Mesaj msj : list1)
            if (list2.contains(msj))
                v1.add(msj);

        List<Mesaj> list3 = service_mesaj.sentMessages(otherId);
        List<Mesaj> list4 = service_mesaj.receivedMessages(this.user.getId());
        Vector<Mesaj> v2 = new Vector<Mesaj>();
        for (Mesaj msj : list3)
            if (list4.contains(msj))
                v2.add(msj);

        Vector<Mesaj> vFin = v1;
        vFin.addAll(v2);


        Collections.sort(vFin, new Comparator<Mesaj>() {
            @Override
            public int compare(Mesaj one,Mesaj other){
                if (one.getDate().isBefore(other.getDate()))
                    return -1;
                if (one.getDate().isEqual(other.getDate()))
                    return 0;
                return 1;
            }

        });

        for (Mesaj mesaj : vFin) {
            //System.out.println(mesaj);
            System.out.println(formatMessage(mesaj));
        }
    }

    private String formatMessage(Mesaj mesaj) {
        String rez = "<" + service_universal_user.getById(mesaj.getSenderId()).getUsername();
        rez += " -> ";

        for (Long ids : mesaj.getReceiversId())
            rez += service_universal_user.getById(ids).getUsername() + ", ";

        rez +="> ";
        rez += "\n>" + mesaj.getMessage();

        return rez;
    }

    private void uiShowSentMessages() {
        for ( Mesaj msj : service_mesaj.sentMessages(this.user.getId()))
        {
            System.out.println(formatMessage(msj));
        }
    }

    private void uiMessageShow(){
        System.out.println("Showing message inbox..");
        for ( Mesaj msj : service_mesaj.receivedMessages(this.user.getId()))
        {
            System.out.println(formatMessage(msj));
        }
        System.out.println("1 -> Raspunde unui prieten");
        System.out.println("(Apasa tasta Enter pentru a te intoarce la meniul principal)");
        printConsoleMarker();
        if (readInput().equals("1"))
            uiReply();
    }

    private void uiReply() {
        System.out.println("Introdu id-ul persoanei careia doresti sa ii raspunzi: ");
        printConsoleMarker();
        Long toId = Long.parseLong(readInput());
        Vector<Long> vToId = new Vector<Long>();
        vToId.add(toId);


        System.out.println("Scrie mesajul pe care doresti sa il trimiti");
        String inputMsj = readInput();

        Long responseTo = -1L;

        Mesaj lastMessage = new Mesaj();

        for ( Mesaj msj : service_mesaj.receivedMessages(this.user.getId()))
        {
            if (msj.getSenderId() == toId)
                lastMessage = msj;
        }

        for (Long ids : lastMessage.getReceiversId())
            if (ids != this.user.getId())
                vToId.add(ids);

        List<Long> recievedIds = vToId.stream().collect(Collectors.toList());
        Mesaj msj = new Mesaj(this.user.getId(), recievedIds, inputMsj, lastMessage.getId());

        System.out.println("Input: " + msj.toString());

        service_mesaj.addUMesaj(msj);

    }

    private void uiRequestShow(){
        System.out.println("Showing friend requests..");
        int requestIndex = 0;

        List<Prietenie> requestList = service_universal_friend.specificFriendRequests(this.user.getId());
        for (Prietenie prietenie : requestList)
            System.out.println("" + (++requestIndex) + ". " + prietenie.showyFormat());

        System.out.println("Pentru a raspunde la o cerere, introduceti numarul cererii si 0(acceptare), 1(refuzare)" +
                            " separate prin ;");

        String attr = readInput();
        if(attr.matches("[0-9]*;[0-9]*"))
        {
            List<String> attrSplited = Arrays.asList(attr.split(";"));

            int specificRequestIndex = Integer.parseInt(attrSplited.get(0));
            int newStatus = Integer.parseInt(attrSplited.get(1));

            service_universal_friend.updateFriendRequest(requestList.get(specificRequestIndex - 1).getId().getLeft(),
                                                         requestList.get(specificRequestIndex - 1).getId().getRight(),
                                                         newStatus);

        }
        else
            System.out.println("returning to main menu..");
    }

    private void uiMessage(){
        System.out.println("Drafting new message..");

        System.out.println("Scrie id-urile celor ce vrei sa le trimiti mesajul separate prin ';'");
        printConsoleMarker();
        String inputIds = readInput();
        List<String> parsedIds = Arrays.asList(inputIds.split(";"));

        List<Long> recievedIds = parsedIds.stream().map(Long::parseLong).collect(Collectors.toList());
        System.out.println("Scrie mesajul pe care doresti sa il trimiti");
        String inputMsj = readInput();

        Long responseTo = -1L;

        Mesaj msj = new Mesaj(this.user.getId(), recievedIds, inputMsj, responseTo);
        System.out.println("Input: " + msj.toString());

        service_mesaj.addUMesaj(msj);

    }

    private void uiShowSpecificFriends() {

        System.out.println("Loading friends list..");
        System.out.println("Friends list:");
        for(Long friendship : service_universal_friend.getSpecificFriends(this.user.getId()))
            System.out.println("    " + friendship);

    }



    //service_universal_friend.getSpecificFriends(10L/*service_universal_user.getId(attr.get(0), attr.get(1))*/).forEach(System.out::println);
    /**service_universal_friend.getSpecificFriends(service_universal_user.getId(attr.get(0), attr.get(1)))
     .forEach(friendship -> System.out.println(service_universal_user.getById(friendship).getFirstName() +
     ";" + service_universal_user.getById(friendship).getLastName() ));
     */
    private void uiAddFriend()  {
        System.out.println("Preparing friend request..");

        System.out.println("Introduceti id-ul persoanei careia doriti sa ii trimiteti cererea de prietenie: ");
        Long idReceiver = Long.parseLong(readInput());

        Prietenie potentialFriendship = new Prietenie(-1);
        potentialFriendship.setId(new Tuple<Long, Long>(this.user.getId(), idReceiver));

        //System.out.println(potentialFriendship.showyFormat());

        service_universal_friend.addPrietenie(potentialFriendship);
    }

    /** uiAdd, uiDel, uiShow, uiQ*/

    private void uiQ()
    {
        System.out.println("Closing..");
        System.exit(0);
    }


    private void uiShow() {
        System.out.println("Utilizator curent: " + this.user.getUsername());
    }
}
