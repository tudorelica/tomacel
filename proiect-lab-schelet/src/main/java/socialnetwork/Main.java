package socialnetwork;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.RepositoryDb;
import socialnetwork.repository.database.EvenimenteDbRepo;
import socialnetwork.repository.database.MesajeDbRepository;
import socialnetwork.repository.database.PrieteniiDbRepository;
import socialnetwork.repository.database.UtilizatorDbRepository;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.repository.file.PrietenFile;
import socialnetwork.service.EvenimenteServiceDb;
import socialnetwork.service.MesajServiceDb;
import socialnetwork.service.PrietenServiceDb;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {

        //UiGeneral gen_ui = new UiGeneral();
        //gen_ui.Run();


        /**Databases*/
        final String url = "jdbc:postgresql://localhost:5432/Tomacel2";
        final String username= "postgres";
        final String password= "hordepunk11";



        RepositoryDb <Long, Utilizator> test_db_repo = new UtilizatorDbRepository(url, username, password, new Validator<Utilizator>() {
            @Override
            public void validate(Utilizator entity) throws ValidationException {

            }
        });

        RepositoryDb<Tuple<Long, Long>, Prietenie> test_prietenie_db_repo = new PrieteniiDbRepository(url, username, password, new Validator<Prietenie>() {
            @Override
            public void validate(Prietenie entity) throws ValidationException {

            }
        });

        Repository<Tuple<Long, Long>, Prietenie> fill_repo = new PrietenFile(".\\data\\friends.csv", new Validator<Prietenie>() {
            @Override
            public void validate(Prietenie entity) throws ValidationException {

            }
        });

//        //UtilizatorServiceDb serviceDb = new UtilizatorServiceDb(test_db_repo);
//
//        for (Utilizator u : serviceDb.getAll())
//            System.out.println(u);

        System.out.println("--> " + test_db_repo.findOne(20L));

        Utilizator test_user = new Utilizator("Marian", "Coman", "user21", "parola21");
        test_user.setId(21L);

        /*
        for (Prietenie u : fill_repo.findAll()) {
            System.out.println("trying to save: " + u);
            System.out.println(test_prietenie_db_repo.save(u));
        }*/



        Prietenie prietenie_test = new Prietenie(0);
        prietenie_test.setId(new Tuple<Long, Long>(21L, 10L));
        prietenie_test.setDate(LocalDateTime.now());

        PrietenServiceDb prietenDb = new PrietenServiceDb(test_prietenie_db_repo);

        //prietenDb.updateFriendRequest(21L, 12L,0);

        Repository<Long, Mesaj> msg_fill_repo = new MesajFile(".\\data\\messages.csv", new Validator<Mesaj>() {
            @Override
            public void validate(Mesaj entity) throws ValidationException {

            }
        });



        RepositoryDb<Long, Mesaj> mesajeDb = new MesajeDbRepository(url, username, password, new Validator<Mesaj>() {
            @Override
            public void validate(Mesaj entity) throws ValidationException {

            }
        });

        //System.out.println(mesajeDb.findOne(-7745472442710579141L));

        List<Long> list = new ArrayList<Long>();
        list.add(10L);
        //5;1;10;Salut GagiQ;2020-12-02T13:16:29.579815600;-1;-7745472442710579141;
        Mesaj test_mesaj = new Mesaj(5L, list, "QQQQ",-1L);
        test_mesaj.setDate(LocalDateTime.now());
        test_mesaj.setId(-7745472446710579141L);

        //System.out.println(mesajeDb.save(test_mesaj));
        //System.out.println(mesajeDb.findOne(-7745472442710579141L));
        //System.out.println(mesajeDb.delete(-7745472442710579141L));
        //System.out.println(mesajeDb.findOne(-7745472442710579141L));


        //for (Mesaj m : msg_fill_repo.findAll())
        //    System.out.println(mesajeDb.save(m));


        //for (Mesaj m : mesajeDb.findAll())
            //System.out.println(m);

        MesajServiceDb mesajServiceDb = new MesajServiceDb(mesajeDb);

        //System.out.println(mesajServiceDb.delMesaj(test_mesaj.getId()));

//        System.out.println(">>>>>>>>>>>>>>>>>>>>>\n\n");
//
//        for (Mesaj m : mesajServiceDb.getAll())
//            System.out.println(m);

        System.out.println(">>>>>>>>>>>>>>>>>>>>>\n\n");

        //System.out.println(mesajServiceDb.addUMesaj(test_mesaj));
        //System.out.println(mesajServiceDb.delMesaj(test_mesaj.getId()));

//        for (Mesaj m : mesajServiceDb.sentMessages(5L))
//            System.out.println(m);
//
//
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>\n\n");
//
//        System.out.println(mesajServiceDb.delMesaj(test_mesaj.getId()));
//
//        for (Mesaj m : mesajServiceDb.getAll())
//            System.out.println(m);


        LocalDate date2= LocalDate.of(2021, 1, 13);
        LocalTime time = LocalTime.of(23,00);
        LocalDateTime date = LocalDateTime.of(date2, time);


        CustomEvent event_test = new CustomEvent(date, 10l, "Movie-night 2", "C8ii, ne uitam la filme HoRroR :o!");
        event_test.setCurrentDate(LocalDateTime.now());
        event_test.setId(3L);
        event_test.addId(12l);
        event_test.addId(5l);
        event_test.addId(74l);
        event_test.addId(14l);

        //event_test.verifyForAnHour();

        //System.out.println(event_test);


        RepositoryDb<Long, CustomEvent> repo_events = new EvenimenteDbRepo(url, username, password, new Validator<CustomEvent>() {
            @Override
            public void validate(CustomEvent entity) throws ValidationException {

            }
        });

        //repo_events.save(event_test);
        EvenimenteServiceDb evenimenteServiceDb = new EvenimenteServiceDb(repo_events);

//        for (CustomEvent e : repo_events.findAll())
//        {
//            System.out.println(e);
//        }
//
//        System.out.println("VVVV SERVICE VVVV");
//
//        for (CustomEvent e : evenimenteServiceDb.(74L))
//        {
//            System.out.println(e);
//        }
//
//        System.out.println("VVVV Other VVVV");

        //System.out.println(evenimenteServiceDb.getEventsByUserId(10L);

        //System.out.println(repo_events.findOne(1l));
        //System.out.println('\n');

        //repo_events.save(event_test);

        MainFX.main(args);
    }
}


