package socialnetwork.domain;

public class CererePrietenieDTO {

    public String from;
    public String towards;
    public Long fromId;
    public String dateAsString;

    public CererePrietenieDTO(String from,Long fromId, String towards, String dateAsString)
    {
        this.from = from;
        this.towards = towards;
        this.fromId = fromId;
        this.dateAsString = dateAsString;
    }
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTowards() {
        return towards;
    }

    public void setTowards(String towards) {
        this.towards = towards;
    }

    public String getDateAsString() {
        return dateAsString;
    }

    public Long getFromId() {
        return fromId;
    }


    public void setDateAsString(String dateAsString) {
        this.dateAsString = dateAsString;
    }

    @Override
    public String toString() {
        return "PrietenieDTO{" +
                "From:'" + from + '\'' +
                "Towards:'" + towards + '\'' +
                "Date" + dateAsString + '\'' +
                '}';
    }
}
