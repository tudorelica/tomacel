package socialnetwork.domain;

public class CustomEventDTO {
    public Long id;
    public String titlu;
    public String organizator;
    public String data;
    public String particip;

    public CustomEventDTO(Long id, String titlu, String organizator, String data, boolean ok) {
            this.id = id;
            this.titlu = titlu;
            this.organizator = organizator;
            this.data = data;
            if (ok)
                this.particip = "Da";
            else
                this.particip = "Nu";
    }

    public CustomEventDTO(Long id, String titlu, String organizator, String data) {
        this.id = id;
        this.organizator = organizator;
        this.titlu = titlu;
        this.data = data;

        particip = "-";
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getOrganizator() {
        return organizator;
    }

    public void setOrganizator(String organizator) {
        this.organizator = organizator;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getParticip() {
        return particip;
    }

    public void setParticip(String particip) {
        this.particip = particip;
    }



}
