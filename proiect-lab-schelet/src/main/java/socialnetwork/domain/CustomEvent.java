package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CustomEvent extends Entity<Long>{

    public LocalDateTime targetDate;
    public LocalDateTime currentDate;
    public Long creatorId;
    public String titlu;
    public String descriere;
    public String ids = "";

    public CustomEvent(LocalDateTime targetDate, Long creatorId, String titlu, String descriere){
        this.targetDate = targetDate;
        this.creatorId = creatorId;
        this.titlu = titlu;
        this.descriere = descriere;
        addId(creatorId);
    }

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    /**Setters*/
    public void setCurrentDate(LocalDateTime currentDate)
    {
        this.currentDate = currentDate;
    }

    public String setString(String ids)
    {
       this.ids = ids;

        return ids;
    }

    public String setTitlu(String titlu)
    {
        this.titlu = titlu;
        return titlu;
    }

    public String setDesc  (String descriere)
    {
        this.descriere = descriere;
        return descriere;
    }

    /**Getters*/

    public String getStringIds()
    {
        return ids;
    }

    public Iterable<Long> getIds()
    {
        List<Long> parsedIds = Arrays.asList(ids.split(";")).stream().map(x -> Long.parseLong(x)).collect(Collectors.toList());
        return parsedIds;
    }

    public Long getCreatorId()
    {
        return creatorId;
    }

    public String getTitlu()
    {
        return titlu;
    }

    public String getDescriere()
    {
        return descriere;
    }

    public LocalDateTime getTargetDate(){
        return targetDate;
    }

    public LocalDateTime getCurrentDate(){
        return currentDate;
    }

    /**Crude operations*/
    public String addId(Long id)
    {
        return (ids = ids + id + ";");
    }

    public boolean participId(Long userId)
    {
        for (Long l : getIds())
            if (l == userId)
                return true;

        return false;
    }

    public String removeId(Long id)
    {
        List<Long> parsedIds = Arrays.asList(ids.split(";")).stream().map(x -> Long.parseLong(x)).collect(Collectors.toList());

        String rez = "";
        for(Long elem : parsedIds)
            if (elem != id)
                rez = rez + elem + ";";

        return (ids = rez);
    }

    public void verifyForAnHour() {
//        LocalDate date2= LocalDate.of(2021, 1, 10);
//        LocalTime time = LocalTime.of(12,20);
//        targetDate = LocalDateTime.of(date2, time);



        final Runnable beeper = new Runnable() {
            public void run() { System.out.println("beep");
                //System.out.println("current: " + currentDate);
                //System.out.println("target: " + targetDate);
                if ((targetDate.getYear() == currentDate.getYear()) && (targetDate.getMonth() == currentDate.getMonth()) &&
                        (targetDate.getDayOfMonth() == currentDate.getDayOfMonth()))
                {
                    //System.out.println("E ziua petrekerii!!!");
                }
                else
                {
                    //System.out.println("Nu e chiar ziua petrekerii :/");
                }
            }
        };
        final ScheduledFuture<?> beeperHandle =
                scheduler.scheduleAtFixedRate(beeper, 10, 10, SECONDS);
        scheduler.schedule(new Runnable() {
            public void run() { beeperHandle.cancel(true); }
        }, 60 * 60, SECONDS);
    }

    /**ToString()*/

    @Override
    public String toString(){
        return "<Event><id: " + this.getId() + "| titlu:" + this.getTitlu()
                + "| descriere: " + this.getDescriere()
                + "| iduriParticipanti: " + this.getIds()
                + "| currentDate: " + this.getCurrentDate()
                + "| targetDate" + this.getTargetDate()
                + ">";
    }
}
