package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class Mesaj extends Entity<Long>{
    private Long senderId;
    private List<Long> receiversId;
    private String message;
    private LocalDateTime date;
    private Long responseTo;

    public Mesaj () {
        responseTo = -1L;
        date = LocalDateTime.now();
        this.setId(new Random().nextLong());
    };

    public Mesaj(Long sender, List<Long> receivers, String message, Long responseTo){
        this.senderId = sender;
        this.receiversId = receivers;
        this.message = message;
        this.date = LocalDateTime.now();
        this.responseTo = responseTo;
        this.setId(new Random().nextLong());
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long sender) {
        this.senderId = sender;
    }

    public List<Long> getReceiversId() {
        return receiversId;
    }

    public void setReceiversId(List<Long> receivers) {
        this.receiversId = receivers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getResponseTo() {
        return responseTo;
    }

    public void setResponseTo(Long responseTo) {
        this.responseTo = responseTo;
    }


    @Override
    public String toString()
    {
        String result = "" + senderId + ";" + receiversId.size();

        for (Long elem : receiversId)
            result += ";" + elem;

        result += ";" + message;

        result += ";" + date.toString() + ";"+ responseTo + ";" + this.getId() + ";" ;

        return result;
    }





}
