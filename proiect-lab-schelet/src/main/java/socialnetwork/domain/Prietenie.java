package socialnetwork.domain;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;
    private int status;     /** -1 -> cerere de prietenie in asteptarea unui raspuns
                        0 -> cerere de prietenie acceptata
                        1 -> cerere de prietenie neacceptata
                    */
    public Prietenie() {
        date = LocalDateTime.now();
    }

    public Prietenie( int newStatus) {
        date = LocalDateTime.now();
        status = newStatus;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    /**
     *
     * @sets the date when the friendship was created
     * @since LAB4 | Saptamana 8 | 18.11.2020
     */
    public void setDate(LocalDateTime new_date) {date = new_date;}

    public void setStatus(int newStatus) {
        this.status = newStatus;
    }

    public int getStatus(){
        return this.status;
    }


    @Override
    /**
    *
    * @return the freindship formatted into a string: "example_friend_id1;example_friend_id2"
    * */
    public String toString()
    {
        //return getId().toString();
        return "" + this.getId().getLeft()
                + ";" + this.getId().getRight()
                + ";" + this.getDate().toString()
                + ";" + this.getStatus();
    }

    public String showyFormat()
    {
        //return getId().toString();
        if (this.status == -1)
            return "the user with the id: " + this.getId().getLeft()
                + " wants to be your friend";

        if(this.status == 0)
            return "you and the user with the id: " + this.getId().getLeft()
                    + " are friends";
        else
            return "the friend request which involves " + this.getId().getLeft()
                    + " has been declined";
    }
}
