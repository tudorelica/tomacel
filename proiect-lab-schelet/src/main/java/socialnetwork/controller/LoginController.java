package socialnetwork.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Transition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryDb;
import socialnetwork.repository.database.EvenimenteDbRepo;
import socialnetwork.repository.database.MesajeDbRepository;
import socialnetwork.repository.database.PrieteniiDbRepository;
import socialnetwork.repository.database.UtilizatorDbPageRepository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.EvenimenteServiceDb;
import socialnetwork.service.MesajServiceDb;
import socialnetwork.service.PrietenServiceDb;
import socialnetwork.service.UtilizatorServiceDb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class LoginController {
    public TableColumn<Utilizator, String> pagination_user_column;
    public TableColumn<Utilizator, String> pagination_nume_column;
    public TableColumn<Utilizator, String> pagination_prenume_column;


    ObservableList<CererePrietenieDTO> friendshipList = FXCollections.observableArrayList();
    ObservableList<UtilizatorDTO> friends_list = FXCollections.observableArrayList();
    ObservableList<Utilizator> user_list = FXCollections.observableArrayList();
    ObservableList<CustomEventDTO> events_list = FXCollections.observableArrayList();
    List<CustomEvent> events_reminder_list = new ArrayList<CustomEvent>();

    private UtilizatorServiceDb service_universal_user;
    private PrietenServiceDb service_universal_friend;
    private MesajServiceDb service_mesaj;
    private EvenimenteServiceDb service_eveniment;

    private Utilizator user = new Utilizator("basic", "basic","basic","basic");
    private Utilizator user2 = new Utilizator("basic", "basic","basic","basic");


    /**Databases*/
    final String url = "jdbc:postgresql://localhost:5432/Tomacel2";
    final String username= "postgres";
    final String password= "hordepunk11";

    @FXML
    AnchorPane anchor_login;

    @FXML
    StackPane stack_1;

    @FXML
    StackPane stack_2;

    @FXML
    StackPane message_input_stackpane;

    @FXML
    StackPane message_output_stackpane;

    @FXML
    Label user_label;

    @FXML
    Label date_label;

    @FXML
    Label hour_label;

    @FXML
    Label requests_label;

    @FXML
    Label friends_label;

    @FXML
    Label pending_label;

    @FXML
    Label reports_label;

    @FXML
    Label events_label;

    @FXML
    Label users_label;

    @FXML
    TextField text_field;

    @FXML
    TextField event_field_1;

    @FXML
    TextField event_field_2;

    @FXML
    TextField message_field;

    @FXML
    ImageView button_accept;

    @FXML
    ImageView button_decline;

    @FXML
    ImageView button_remove;

    @FXML
    ImageView button_message;

    @FXML
    ImageView button_generate_left;

    @FXML
    ImageView button_generate_right;

    @FXML
    ImageView button_export_left;

    @FXML
    ImageView button_export_right;

    @FXML
    ImageView button_particip;

    @FXML
    ImageView button_stau_acasa;

    @FXML
    ImageView event_generate_button;

    @FXML
    TableView<CererePrietenieDTO> report_friends_table;

    @FXML
    TableColumn<CererePrietenieDTO, String> report_friends_column;

    @FXML
    PasswordField pass_field;

    @FXML
    StackPane stack_pane_dash;

    @FXML
    BarChart bar_chart_1;

    @FXML
    BarChart bar_chart_2;

    @FXML
    AnchorPane report_left_anchor;

    @FXML
    AnchorPane report_right_anchor;

    @FXML
    AnchorPane events_pane;

    @FXML
    AnchorPane pagination_pane;

    @FXML
    DatePicker date_picker_left_1;

    @FXML
    DatePicker date_picker_left_2;

    @FXML
    DatePicker date_picker_right_1;

    @FXML
    DatePicker date_picker_right_2;

    @FXML
    DatePicker event_picker;

    @FXML
    TableColumn<CererePrietenieDTO, String> from_column;

    @FXML
    TableColumn<CererePrietenieDTO, String> date_column;

    @FXML
    TableView<CererePrietenieDTO> requests_table;

    @FXML
    TableView<CustomEventDTO> events_table;

    @FXML
    TableView<Utilizator> pagination_table;

    @FXML
    Pagination pagination_pagination;

    @FXML
    TableColumn<CustomEventDTO, String> titlu_column;

    @FXML
    TableColumn<CustomEventDTO, String> organizator_column;

    @FXML
    TableColumn<CustomEventDTO, String> data_column;

    @FXML
    TableColumn<CustomEventDTO, String> particip_column;


    /****************************************************************/
    /************** Initial-Ran Function ********************/
    /****************************************************************/

    @FXML
    protected void initialize()
    {
        System.out.println("<User Ui>");

        String fileName=".\\data\\users.csv";
        String fileName2=".\\data\\friends.csv";
        String fileName3=".\\data\\messages.csv";

        setGlobalEventHandler(stack_1);
        message_field_entered();

        bar_chart_1.lookup(".chart-plot-background").setStyle("-fx-background-color:  rgba(0,168,355,0.50);");
        bar_chart_2.lookup(".chart-plot-background").setStyle("-fx-background-color:  rgba(0,168,355,0.50);");

       // message_field.setText("  ");


      /**  Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileName
                , new UtilizatorValidator());*/

        PagingRepository<Long, Utilizator> test_db_repo = new UtilizatorDbPageRepository(url, username, password, new UtilizatorValidator());



        RepositoryDb<Tuple<Long, Long>, Prietenie> prietenRepository = new PrieteniiDbRepository(url, username, password,
                new Validator<Prietenie>() {
                    @Override
                    public void validate(Prietenie entity) throws ValidationException {

                    }
                });


        RepositoryDb<Long, Mesaj> mesajeDb = new MesajeDbRepository(url, username, password, new Validator<Mesaj>() {
            @Override
            public void validate(Mesaj entity) throws ValidationException {

            }
        });

        RepositoryDb<Long, CustomEvent> repo_events = new EvenimenteDbRepo(url, username, password, new Validator<CustomEvent>() {
            @Override
            public void validate(CustomEvent entity) throws ValidationException {

            }
        });



        test_db_repo.findAll().forEach(System.out::println);
        prietenRepository.findAll().forEach(System.out::println);

        //UtilizatorService service = new UtilizatorService(userFileRepository);
        UtilizatorServiceDb service = new UtilizatorServiceDb(test_db_repo);
        PrietenServiceDb service2 = new PrietenServiceDb(prietenRepository);
        MesajServiceDb service3 = new MesajServiceDb(mesajeDb);
        EvenimenteServiceDb service4 = new EvenimenteServiceDb(repo_events);

        service_universal_user = service;
        service_universal_friend = service2;
        service_mesaj = service3;
        service_eveniment = service4;

        pagination_pagination.currentPageIndexProperty().addListener((obs, oldIndex, newIndex) ->
                page_handler());

        Page<Utilizator> users = service_universal_user.getPagedUsers(pagination_pagination.getCurrentPageIndex());
        user_list.setAll(users.getContent().collect(Collectors.toList()));


        pagination_user_column.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("username"));
        pagination_prenume_column.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        pagination_nume_column.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));

        pagination_table.setItems(user_list);

        //stack_pane_dash.setLayoutX(Screen.getScreens().size() / 2);
        stack_pane_dash.setLayoutX(673);
        //System.out.println(Screen.getScreens().size());

        /**from_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("from"));
        date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("dateAsString"));

        requests_table.setItems(friendshipList);*/
    }

    private void setGlobalEventHandler(Node root) {
        root.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                System.out.println("enter pressed");

                try {
                    button_pressed();
                }
                catch(Exception ex)
                {

                }
                ev.consume();
            }
        });
    }



    /****************************************************************/
    /************** Converting to DTO Lists ********************/
    /****************************************************************/

    /** Returns a <UtilizatorDTO> list of the current user's friends*/
    private List<UtilizatorDTO> getPrieteniDTO() {
        // TODO
        List<Long> prieteniIdList = service_universal_friend.getSpecificFriends(this.user.getId());

        return prieteniIdList.stream()
                .map(prieten -> service_universal_user.getById(prieten))
                .map(prieten -> { return new UtilizatorDTO(prieten.getId(),
                        prieten.getFirstName(),
                        prieten.getLastName(),
                        "    " + prieten.getUsername());}
                )
                .collect(toList());
    }

    /** Returns a sketchy <CererePrietenieDTO> list of the current user's friends*/
    private List<CererePrietenieDTO> getPrieteniSketchyDTO() {
        return getPrieteniDTO().stream().map(prieten -> new CererePrietenieDTO("    " + prieten.getFirst_name() + " "
                + prieten.getLast_name()
                , prieten.getId(), "", ""))
                .collect(toList());
    }

    /** Gets <CererePrietenieDTO> list of friendship requests sent by other users to the the current user*/
    private List<CererePrietenieDTO> getCererePrietenieDTO() {
        // TODO
        List<CererePrietenieDTO> CerereDTOlist = new ArrayList<>();
        List<Prietenie> prietenieList = service_universal_friend.specificFriendRequests(this.user.getId());

        return prietenieList.stream()
                .map(prietenie -> { return new CererePrietenieDTO("   " + service_universal_user.getUserById(prietenie.getId().getLeft()),
                        prietenie.getId().getLeft(),
                        service_universal_user.getUserById(prietenie.getId().getRight()),
                        prietenie.getDate().getDayOfMonth() + "." +
                                prietenie.getDate().getMonth() + "." +
                                prietenie.getDate().getYear());}
                )
                .collect(toList());



    }

    private List<CustomEventDTO> getCustomEvents() {
        // TODO
        List<CustomEvent> cList = new ArrayList<CustomEvent>();
        List<CustomEventDTO> rezList = new ArrayList<CustomEventDTO>();

        for (CustomEvent e : service_eveniment.getAllEvents()) {
            cList.add(e);
            System.out.println(e);

            boolean k;
            if(e.participId(user.getId()))
                k = true;
            else
                k = false;

            CustomEventDTO eventDTO = new CustomEventDTO(e.getId(),"   " + e.getTitlu(), service_universal_user.getUserById(e.getCreatorId())
                    ,e.getTargetDate().getDayOfMonth() + "." +
                                e.getTargetDate().getMonthValue() + "." +
                                e.getTargetDate().getYear()
                    ,k);

            System.out.println(eventDTO.getTitlu());
            rezList.add(eventDTO);
        }

//        List<CustomEventDTO> rezList = cList.stream()
//                .map(event -> {
//                    return new CustomEventDTO(
//                            event.getId(),
//                            event.getTitlu(),
//                            service_universal_user.getUserById(event.getCreatorId()),
//                        event.getTargetDate().getDayOfMonth() + "." +
//                                event.getTargetDate().getMonth() + "." +
//                                event.getTargetDate().getYear(),
//                            event.participId(user.getId()));}
//                ).collect(toList());

        for (CustomEventDTO e : rezList) {
            System.out.println("RezList: " + e.titlu);
        }

        return rezList;
    }

    /** Gets <CererePrietenieDTO> list of pending friendship requests sent by the user */
    private List<CererePrietenieDTO> getPendingsPrietenieDTO() {
        // TODO
        List<CererePrietenieDTO> CerereDTOlist = new ArrayList<>();
        List<Prietenie> prietenieList = service_universal_friend.specificFriendPendings(this.user.getId());

        return prietenieList.stream()
                .map(prietenie -> { return new CererePrietenieDTO(service_universal_user.getUserById(prietenie.getId().getLeft()),
                        prietenie.getId().getRight(),
                        "   " + service_universal_user.getUserById(prietenie.getId().getRight()),
                        prietenie.getDate().getDayOfMonth() + "." +
                                prietenie.getDate().getMonth() + "." +
                                prietenie.getDate().getYear());}
                )
                .collect(toList());



    }



    /****************************************************************/
    /************** ACTUAL BUTTONS **********************************/
    /****************************************************************/

    /** login_button Button*/
    public void button_pressed() throws InterruptedException {
        //System.out.println("Hello World!");
        //anchor_login.setVisible(false);

        this.user = service_universal_user.accountCheck(text_field.getText(), pass_field.getText());
        if (this.user == null) {
            pass_field.setText("");
            text_field.setText("Unknown user!");

            System.out.println("Nay");
        }
        else {
            System.out.println("Yay");
            FadeTransition ft = new FadeTransition(Duration.millis(3000), anchor_login);
            ft.setFromValue(1.0);
            ft.setToValue(0.0);
            ft.play();

            stack_2.setVisible(true);
            FadeTransition ft2 = new FadeTransition(Duration.millis(4500), stack_2);
            ft2.setFromValue(0.0);
            ft2.setToValue(1.0);
            ft2.play();

            String date = LocalDateTime.now().getDayOfMonth() + "."
                    + LocalDateTime.now().getMonthValue()
                    + "." + LocalDateTime.now().getYear();

            String hour = LocalDateTime.now().getHour() + ":"
                    + LocalDateTime.now().getMinute();

            //date_label.setText(date_label.getText() + " " + date);

            String content = date_label.getText() + " " + date;
            String content2 = hour_label.getText() + " " + hour;
            String content3 = user_label.getText() + " " + this.user.getFirstName() + " " + this.user.getLastName();

            final Animation animation = new Transition() {
                {
                    setCycleDuration(Duration.millis(6000));
                }

                protected void interpolate(double frac) {
                    final int length = content.length();
                    final int n = Math.round(length * (float) frac);
                    date_label.setText(content.substring(0, n));
                }
            };

            final Animation animation2 = new Transition() {
                {
                    setCycleDuration(Duration.millis(3000));
                }

                protected void interpolate(double frac) {
                    final int length = content2.length();
                    final int n = Math.round(length * (float) frac);
                    hour_label.setText(content2.substring(0, n));
                }
            };

            final Animation animation3 = new Transition() {
                {
                    setCycleDuration(Duration.millis(4000));
                }

                protected void interpolate(double frac) {
                    final int length = content3.length();
                    final int n = Math.round(length * (float) frac);
                    user_label.setText(content3.substring(0, n));
                }
            };

            animation.play();
            animation2.play();
            animation3.play();

            for (CustomEvent e : service_eveniment.getEventsByUserId(user.getId()))
            {
                System.out.println("now(): "
                        + LocalDateTime.now().getMonthValue()
                        + " " + LocalDateTime.now().getDayOfMonth()
                        + " " + LocalDateTime.now().getYear());

                System.out.println("target(): "
                        + e.getTargetDate().getMonthValue()
                        + " " + e.getTargetDate().getDayOfMonth()
                        + " " + e.getTargetDate().getYear());

                if ((LocalDateTime.now().getMonthValue() == e.getTargetDate().getMonthValue())
                    && (LocalDateTime.now().getDayOfMonth() == e.getTargetDate().getDayOfMonth())
                        && (LocalDateTime.now().getYear() == e.getTargetDate().getYear())
                ) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Eveniment astazi: <" + e.getTitlu() + ">", ButtonType.OK);
                    alert.showAndWait();

                }
                else
                {
                    System.out.println("MAI E");
                }
            }
        }
        //  TimeUnit.SECONDS.sleep(1);
        //  stack_1.setVisible(false);
        //  stack_2.setVisible(true);

    }



    /****************************************************************/
    /************** LABELS (used as Buttons) *******************/
    /****************************************************************/

    /** requests_label Label*/
    public void mouse_moved() {
        requests_label.setTextFill(Paint.valueOf("#7cd182"));
        requests_label.setText("<" + requests_label.getText() + ">");
    }

    public void mouse_left() {
        requests_label.setTextFill(Paint.valueOf("#0cb91a"));

        requests_label.setText("Requests");
    }

    public void requests_label_clicked(){
        System.out.println("requests_label_clicked()!");
        if ((requests_table.isVisible()) && (from_column.getText().equals("From:"))) {
            requests_table.setVisible(false);
            button_accept.setVisible(false);
            button_decline.setVisible(false);
            message_field.setVisible(false);
        }
        else {



            from_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("from"));
            date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("dateAsString"));

            requests_table.setItems(friendshipList);

            requests_table.setVisible(true);
            button_remove.setVisible(false);
            button_message.setVisible(false);
            button_accept.setVisible(true);
            button_decline.setVisible(true);
            message_input_stackpane.toBack();
            message_field.setVisible(false);
            report_left_anchor.setVisible(false);
            report_right_anchor.setVisible(false);
            events_pane.setVisible(false);

            from_column.setText("From:");
            from_column.getStyleClass().add("name-column");
            date_column.getStyleClass().add("name-column");
            friendshipList.setAll(getCererePrietenieDTO());
            friendshipList.forEach(System.out::println);
        }
    }


    /** friends_label Label*/
    public void mouse_moved_friends() {
        friends_label.setTextFill(Paint.valueOf("#7cd182"));
        friends_label.setText("<" + friends_label.getText() + ">");
    }

    public void mouse_left_friends() {
        friends_label.setTextFill(Paint.valueOf("#0cb91a"));

        friends_label.setText("Friends");
    }

    public void friends_label_clicked(){
        System.out.println("requests_label_clicked()!");
        if (requests_table.isVisible() && from_column.getText().equals("Friends:")) {
            requests_table.setVisible(false);
            button_message.setVisible(false);
            message_field.setVisible(false);

        }
        else {
            from_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("from"));
            date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>(""));

            requests_table.setItems(friendshipList);

            button_message.setVisible(true);
            button_remove.setVisible(false);
            button_accept.setVisible(false);
            button_decline.setVisible(false);
            message_input_stackpane.toBack();
            message_field.setVisible(false);
            report_left_anchor.setVisible(false);
            report_right_anchor.setVisible(false);
            events_pane.setVisible(false);

            requests_table.setVisible(true);
            from_column.getStyleClass().add("name-column");
            from_column.setText("Friends:");
            date_column.setText("");
            date_column.getStyleClass().add("name-column");

            friendshipList.removeAll();
            friendshipList.setAll(getPrieteniSketchyDTO());
            friendshipList.forEach(System.out::println);
        }
    }


    /** pending_label Label*/
    public void mouse_moved_pending() {
        pending_label.setTextFill(Paint.valueOf("#7cd182"));
        pending_label.setText("<" + pending_label.getText() + ">");
    }

    public void mouse_left_pending() {
        pending_label.setTextFill(Paint.valueOf("#0cb91a"));

        pending_label.setText("Pending");
    }

    public void pending_label_clicked(){
        System.out.println("requests_label_clicked()!");
        if (requests_table.isVisible() && from_column.getText().equals("Towards:")) {
            requests_table.setVisible(false);
            button_remove.setVisible(false);
            message_field.setVisible(false);
        }
        else {
            from_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("towards"));
            date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("dateAsString"));

            requests_table.setItems(friendshipList);

            button_remove.setVisible(true);
            button_accept.setVisible(false);
            button_decline.setVisible(false);
            button_message.setVisible(false);
            message_input_stackpane.toBack();
            message_field.setVisible(false);
            report_left_anchor.setVisible(false);
            report_right_anchor.setVisible(false);
            events_pane.setVisible(false);

            requests_table.setVisible(true);
            from_column.getStyleClass().add("name-column");
            from_column.setText("Towards:");
            date_column.getStyleClass().add("name-column");

            friendshipList.removeAll();
            friendshipList.setAll(getPendingsPrietenieDTO());
            friendshipList.forEach(System.out::println);
        }
    }

    /**reports_label Label*/
    public void mouse_moved_reports() {
        reports_label.setTextFill(Paint.valueOf("#7cd182"));
        reports_label.setText("<" + reports_label.getText() + ">");
    }

    public void mouse_left_reports() {
        reports_label.setTextFill(Paint.valueOf("#0cb91a"));

        reports_label.setText("Reports");
    }

    public void reports_label_clicked(){
        System.out.println("reports_label_clicked()!");
        events_pane.setVisible(false);
        if (requests_table.isVisible() || report_left_anchor.isVisible()) {
            requests_table.setVisible(false);
            button_remove.setVisible(false);
            message_field.setVisible(false);
            button_accept.setVisible(false);
            button_decline.setVisible(false);
            button_message.setVisible(false);
            report_left_anchor.setVisible(false);
            report_right_anchor.setVisible(false);
            events_pane.setVisible(false);
        }
        else {
            report_left_anchor.setVisible(true);
            report_right_anchor.setVisible(true);

            report_friends_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("from"));


            report_friends_table.setItems(friendshipList);


            report_friends_column.getStyleClass().add("name-column");
            report_friends_column.setText("Friends:");


            friendshipList.removeAll();
            friendshipList.setAll(getPrieteniSketchyDTO());

            friendshipList.forEach(System.out::println);
        }
    }

    /**events_label Label*/
    public void mouse_moved_events() {
        events_label.setTextFill(Paint.valueOf("#7cd182"));
        events_label.setText("<" + events_label.getText() + ">");
    }

    public void mouse_left_events() {
        events_label.setTextFill(Paint.valueOf("#0cb91a"));

        events_label.setText("Events");
    }

    public void events_label_clicked(){
        System.out.println("events_label_clicked()!");
        requests_table.setVisible(false);
        button_remove.setVisible(false);
        button_decline.setVisible(false);
        button_message.setVisible(false);
        button_accept.setVisible(false);
        button_decline.setVisible(false);
        //message_input_stackpane.toBack();
        message_field.setVisible(false);
        report_left_anchor.setVisible(false);
        report_right_anchor.setVisible(false);
        if (events_pane.isVisible()) {
            events_pane.setVisible(false);
        }
        else {
            events_pane.setVisible(true);

            titlu_column.setCellValueFactory(new PropertyValueFactory<CustomEventDTO, String>("titlu"));
            organizator_column.setCellValueFactory(new PropertyValueFactory<CustomEventDTO, String>("organizator"));
            data_column.setCellValueFactory(new PropertyValueFactory<CustomEventDTO, String>("data"));
            particip_column.setCellValueFactory(new PropertyValueFactory<CustomEventDTO, String>("particip"));
            //date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("dateAsString".));

            events_table.setItems(events_list);


            from_column.setText("Friends:");
            date_column.setText("");

            events_list.removeAll();
            events_list.setAll(getCustomEvents());
            events_list.forEach(System.out::println);
        }
    }

    /**users_label Label*/
    public void mouse_moved_users() {
        users_label.setTextFill(Paint.valueOf("#7cd182"));
        users_label.setText("<" + users_label.getText() + ">");
    }

    public void mouse_left_users() {
        users_label.setTextFill(Paint.valueOf("#0cb91a"));

        users_label.setText("Users");
    }

    public void users_label_clicked(){
        System.out.println("users_label_clicked()!");

        events_pane.setVisible(false);
        requests_table.setVisible(false);
        button_remove.setVisible(false);
        button_decline.setVisible(false);
        button_message.setVisible(false);
        button_accept.setVisible(false);
        button_decline.setVisible(false);
        //message_input_stackpane.toBack();
        message_field.setVisible(false);
        report_left_anchor.setVisible(false);
        report_right_anchor.setVisible(false);
        if (events_pane.isVisible()) {
            pagination_pane.setVisible(false);
        }
        else {
            pagination_pane.setVisible(true);
        }
    }

    /****************************************************************/
    /************** IMAGEVIEWS (used as Buttons) *******************/
    /****************************************************************
     */
    /** event_generate_button ImageView*/
    public void event_generate_clicked() {

        CustomEvent e = new CustomEvent(LocalDateTime.of(event_picker.getValue(), LocalDateTime.now().toLocalTime()), user.getId(),
                event_field_1.getText(), event_field_2.getText());
        e.setId(ThreadLocalRandom.current().nextLong(100, 10000000));
        service_eveniment.addEvent(e);

        event_field_1.setText("");
        event_field_2.setText("");

        events_list.setAll(getCustomEvents());
        //friendshipList.forEach(System.out::println);
    }

    /** button_accept ImageView*/
    public void accept_clicked() {
        CererePrietenieDTO cerere = requests_table.getSelectionModel().getSelectedItem();
        service_universal_friend.updateFriendRequest(cerere.getFromId(), this.user.getId(), 0);
        friendshipList.setAll(getCererePrietenieDTO());
        friendshipList.forEach(System.out::println);
    }

    public void accept_button_hover(){
        button_accept.setImage(new Image("/LoginView/accept_activated.jpg"));
    }

    public void accept_button_unhover(){
        button_accept.setImage(new Image("/LoginView/button_accept.jpg"));
    }

    public void accept_button_pressed(){
        button_accept.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void accept_button_unpressed(){
        button_accept.setImage(new Image("/LoginView/accept_activated.jpg"));
    }



    /** button_decline ImageView*/
    public void decline_button_hover(){
        button_decline.setImage(new Image("/LoginView/decline_activated.jpg"));
    }

    public void decline_button_unhover(){
        button_decline.setImage(new Image("/LoginView/button_decline.jpg"));
    }

    public void decline_button_pressed(){
        button_decline.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void decline_button_unpressed(){
        button_decline.setImage(new Image("/LoginView/decline_activated.jpg"));
    }

    public void decline_clicked()
    {
        CererePrietenieDTO cerere = requests_table.getSelectionModel().getSelectedItem();
        service_universal_friend.updateFriendRequest(cerere.getFromId(), this.user.getId(), 1);
        friendshipList.setAll(getCererePrietenieDTO());
        friendshipList.forEach(System.out::println);
    }



    /** button_remove ImageView*/
    public void remove_button_hover(){
        button_remove.setImage(new Image("/LoginView/remove_activated.jpg"));
    }

    public void remove_button_unhover(){
        button_remove.setImage(new Image("/LoginView/button_remove.jpg"));
    }

    public void remove_button_pressed(){
        button_remove.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void remove_button_unpressed(){
        button_remove.setImage(new Image("/LoginView/remove_activated.jpg"));
    }

    public void remove_clicked(){
        CererePrietenieDTO cerere = requests_table.getSelectionModel().getSelectedItem();
        service_universal_friend.updateFriendRequest(this.user.getId(), cerere.getFromId(), 1);
        friendshipList.setAll(getPendingsPrietenieDTO());
        friendshipList.forEach(System.out::println);
    }



    /** button_message ImageView*/
    public void message_button_hover(){
        button_message.setImage(new Image("/LoginView/message_activated.jpg"));
    }

    public void message_button_unhover(){
        button_message.setImage(new Image("/LoginView/button_message.jpg"));
    }

    public void message_button_pressed(){
        button_message.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void message_button_unpressed(){
        button_message.setImage(new Image("/LoginView/message_activated.jpg"));
    }

    public void message_clicked(){
        /**CererePrietenieDTO cerere = requests_table.getSelectionModel().getSelectedItem();
        service_universal_friend.updateFriendRequest(this.user.getId(), cerere.getFromId(), 1);
        friendshipList.setAll(getPendingsPrietenieDTO());
        friendshipList.forEach(System.out::println);*/

        if (requests_table.isVisible() && from_column.getText().equals("")) {
            requests_table.setVisible(false);
            button_message.setVisible(false);
        }
        else {
            from_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("from"));
            date_column.setCellValueFactory(new PropertyValueFactory<CererePrietenieDTO, String>("towards"));

            requests_table.setItems(friendshipList);

            button_remove.setVisible(false);
            button_accept.setVisible(false);
            button_decline.setVisible(false);
            message_field.setVisible(true);

            message_input_stackpane.toFront();
            button_message.setVisible(true);

            requests_table.setVisible(true);
            from_column.getStyleClass().add("name-column");
            from_column.setText("");
            date_column.setText("");
            date_column.getStyleClass().add("name-column");
            date_column.getStyleClass().add("my-special-column-style");
            CererePrietenieDTO prieten = requests_table.getSelectionModel().getSelectedItem();

            friendshipList.removeAll();
            System.out.println("" + this.user.getId() + "<------>" + prieten.getFromId());
            user2.setId(prieten.getFromId());
            service_mesaj.conversationGetter(this.user.getId(),prieten.getFromId()).forEach(System.out::println);
            friendshipList.setAll(service_mesaj.conversationGetter(this.user.getId(),prieten.getFromId()));
            friendshipList.forEach(System.out::println);
        }
    }

    /** button_generate_left ImageView*/
    public void generate_left_hover(){
        button_generate_left.setImage(new Image("/LoginView/generate_activated.jpg"));
    }

    public void generate_left_unhover(){
        button_generate_left.setImage(new Image("/LoginView/button_generate.jpg"));
    }

    public void generate_left_pressed(){
        button_generate_left.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void generate_left_unpressed(){
        button_generate_left.setImage(new Image("/LoginView/generate_activated.jpg"));
    }

    public void generate_left_clicked(){
        bar_chart_1.getData().clear();

        XYChart.Series series1 = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();

        series1.setName("Messages");
        series2.setName("Friends");

        LocalDate begin = date_picker_left_1.getValue();
        LocalDate end = date_picker_left_2.getValue();

        List<Mesaj> msg_list = service_mesaj.receivedMessages(user.getId());
        for (Mesaj msg : msg_list)
        {
            System.out.println("Mesaj: " + msg.getMessage() + "; " + msg.getDate());
        }

        for (LocalDate date = begin; date.isBefore(end); date = date.plusDays(1)) {
            LocalDate finalDate = date;
            long nrMessages = msg_list.stream()
                    .filter(message -> message.getDate().getYear() == finalDate.getYear())
                    .filter(message -> message.getDate().getMonth() == finalDate.getMonth())
                    .filter(message -> message.getDate().getDayOfMonth() == finalDate.getDayOfMonth())
                    .collect(toList())
                    .stream().count();



            series1.getData().add(new XYChart.Data(finalDate.toString(), nrMessages));

            int nrFriendRequests = service_universal_friend.getSpecificFriendsObjects(user.getId()).stream()
                    .filter(x -> x.getDate().getDayOfMonth() == finalDate.getDayOfMonth() && x.getDate().getMonthValue()==finalDate.getMonthValue())
                    .collect(Collectors.toMap(x->x.getId(),x->x.getDate()))
                    .size();

            series2.getData().add(new XYChart.Data(finalDate.toString(), nrFriendRequests));
        }

        bar_chart_1.setBarGap(1);
        bar_chart_1.setCategoryGap(10);
        bar_chart_1.getData().addAll(series1);
        bar_chart_1.getData().addAll(series2);
    }

    /** button_generate_right ImageView*/
    public void generate_right_hover(){
        button_generate_right.setImage(new Image("/LoginView/generate_activated.jpg"));
    }

    public void generate_right_unhover(){
        button_generate_right.setImage(new Image("/LoginView/button_generate.jpg"));
    }

    public void generate_right_pressed(){
        button_generate_right.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void generate_right_unpressed(){
        button_generate_right.setImage(new Image("/LoginView/generate_activated.jpg"));
    }

    public void generate_right_clicked(){
        CererePrietenieDTO prieten = report_friends_table.getSelectionModel().getSelectedItem();
        user2.setId(prieten.getFromId());

        bar_chart_2.getData().clear();

        XYChart.Series series1 = new XYChart.Series();

        series1.setName("Messages");

        LocalDate begin = date_picker_right_1.getValue();
        LocalDate end = date_picker_right_2.getValue();

        List<Mesaj> msg_list = service_mesaj.receivedMessages(user.getId());
        for (Mesaj msg : msg_list)
        {
            System.out.println("Mesaj dreapta: " + msg.getMessage() + "; " + msg.getDate());
        }

        for (LocalDate date = begin; date.isBefore(end); date = date.plusDays(1)) {
            LocalDate finalDate = date;
            long nrMessages = msg_list.stream()
                    .filter(message -> message.getDate().getYear() == finalDate.getYear())
                    .filter(message -> message.getDate().getMonth() == finalDate.getMonth())
                    .filter(message -> message.getDate().getDayOfMonth() == finalDate.getDayOfMonth())
                    .filter(message -> message.getSenderId() == user2.getId())
                    .collect(toList())
                    .stream().count();



            series1.getData().add(new XYChart.Data(finalDate.toString(), nrMessages));


        }

        bar_chart_2.setBarGap(1);
        bar_chart_2.setCategoryGap(10);
        bar_chart_2.getData().addAll(series1);
    }

    /** button_export_left ImageView*/
    public void export_left_hover(){
        button_export_left.setImage(new Image("/LoginView/export_activated.jpg"));
    }

    public void export_left_unhover(){
        button_export_left.setImage(new Image("/LoginView/button_export.jpg"));
    }

    public void export_left_pressed(){
        button_export_left.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void export_left_unpressed(){
        button_export_left.setImage(new Image("/LoginView/export_activated.jpg"));
    }

    public void export_left_clicked() throws FileNotFoundException, DocumentException {
        Document doc = new Document();

        PdfWriter.getInstance(doc, new FileOutputStream("MessagesAndFriendships.pdf"));
        doc.open();

        com.itextpdf.text.Font bold = new com.itextpdf.text.Font(Font.FontFamily.HELVETICA, 13, Font.NORMAL);
        Paragraph paragraph = new Paragraph("Messages");


        PdfPTable table = new PdfPTable(5);

        Paragraph paragraph2 = new Paragraph("Friendships");

        PdfPTable table2 = new PdfPTable(1);


        Stream.of("ID", "From", "To", "Text", "Date").forEach(table::addCell);



        List<Mesaj> messageList = StreamSupport.stream(service_mesaj.receivedMessages(user.getId()).spliterator(), false)
                .collect(toList());

        Map<Utilizator, LocalDate> prietenies = service_universal_friend.getSpecificFriendsObjects(user.getId())
                                                .stream()
                                                .map(x -> {if(x.getId().getLeft()==user.getId()) return new Tuple<Long, LocalDate>(x.getId().getRight(),x.getDate().toLocalDate());
                                                        else if(x.getId().getRight()==user.getId()) return new Tuple<Long, LocalDate>(x.getId().getLeft(), x.getDate().toLocalDate());
                                                            return null;})
                                                .collect(Collectors.toMap(x->service_universal_user.getById((x.getLeft())), x->x.getRight()));

        List<Mesaj> messageList1 = new ArrayList<>();
        List<Utilizator> l = new ArrayList<>();
        LocalDate begin = date_picker_left_1.getValue();
        LocalDate end = date_picker_left_2.getValue();
        prietenies.forEach((x, y) -> {
            System.out.println(x.getFirstName() + "|" + x.getLastName() + "|" + y);
        });

        for (LocalDate date = begin; date.isBefore(end); date = date.plusDays(1)) {
            LocalDate finalDate = date;
            messageList1.addAll(messageList.stream()
                    .filter(message -> message.getDate().getDayOfMonth() == finalDate.getDayOfMonth() && message.getDate().getMonthValue()==finalDate.getMonthValue())
                    .collect(toList()) );

            l.addAll(prietenies.entrySet().stream()
                    .filter(x -> x.getValue().getDayOfMonth() == finalDate.getDayOfMonth() && x.getValue().getMonthValue()==finalDate.getMonthValue())
                    .map(x->x.getKey())
                    .collect(toList()));
        }

        messageList1
                .forEach(val -> {
                    table.addCell(val.getId().toString());
                    table.addCell(service_universal_user.getById(val.getSenderId()).getFirstName());
                    table.addCell(service_universal_user.getById(val.getReceiversId().get(0)).getFirstName());
                    table.addCell(val.getMessage());
                    table.addCell(val.getDate().toString());
                });


        paragraph.add(table);
        doc.add(paragraph);
        String prieteni = ": ";
        String result = "";
        l.forEach(System.out::println);
        for (Utilizator utilizator : l) {
            prieteni += utilizator.getFirstName()+" "+utilizator.getLastName()+", ";
        }
        if ((prieteni != null) && (prieteni.length() > 0)) {
            result = prieteni.substring(0, prieteni.length()-2);
        }
        paragraph2.add(result);
        doc.add(paragraph2);

        doc.close();
    }

    /** button_export_right ImageView*/
    public void export_right_hover(){
        button_export_right.setImage(new Image("/LoginView/export_activated.jpg"));
    }

    public void export_right_unhover(){
        button_export_right.setImage(new Image("/LoginView/button_export.jpg"));
    }

    public void export_right_pressed(){
        button_export_right.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void export_right_unpressed(){
        button_export_right.setImage(new Image("/LoginView/export_activated.jpg"));
    }

    public void export_right_clicked() throws DocumentException, FileNotFoundException {
        Document doc = new Document();

        PdfWriter.getInstance(doc, new FileOutputStream("MessagesSpecificFriend.pdf"));
        doc.open();

        com.itextpdf.text.Font bold = new com.itextpdf.text.Font(Font.FontFamily.HELVETICA, 13, Font.NORMAL);
        Paragraph paragraph = new Paragraph("Messages");


        PdfPTable table = new PdfPTable(5);

        CererePrietenieDTO prieten = report_friends_table.getSelectionModel().getSelectedItem();
        user2.setId(prieten.getFromId());


        Stream.of("ID", "From", "To", "Text", "Date").forEach(table::addCell);



        List<Mesaj> messageList = StreamSupport.stream(service_mesaj.receivedMessages(user.getId()).spliterator(), false)
                .collect(toList());


        List<Mesaj> messageList1 = new ArrayList<>();
        List<Utilizator> l = new ArrayList<>();
        LocalDate begin = date_picker_right_1.getValue();
        LocalDate end = date_picker_right_2.getValue();


        for (LocalDate date = begin; date.isBefore(end); date = date.plusDays(1)) {
            LocalDate finalDate = date;
            messageList1.addAll(messageList.stream()
                    .filter(message -> message.getDate().getDayOfMonth() == finalDate.getDayOfMonth() && message.getDate().getMonthValue()==finalDate.getMonthValue())
                    .filter(message -> message.getSenderId() == user2.getId())
                    .collect(toList()) );

        }

        messageList1
                .forEach(val -> {
                    table.addCell(val.getId().toString());
                    table.addCell(service_universal_user.getById(val.getSenderId()).getFirstName());
                    table.addCell(service_universal_user.getById(val.getReceiversId().get(0)).getFirstName());
                    table.addCell(val.getMessage());
                    table.addCell(val.getDate().toString());
                });


        paragraph.add(table);
        doc.add(paragraph);

        doc.close();
    }

    /** button_particip ImageView*/
    public void particip_clicked() {
        CustomEventDTO eventDTO = events_table.getSelectionModel().getSelectedItem();
        service_eveniment.addParticipant(eventDTO.getId(), this.user.getId());
        events_list.setAll(getCustomEvents());
        events_list.forEach(System.out::println);
    }

    public void particip_hover(){
        button_particip.setImage(new Image("/LoginView/particip_activated.jpg"));
    }

    public void particip_unhover(){
        button_particip.setImage(new Image("/LoginView/button_particip.jpg"));
    }

    public void particip_pressed(){
        button_particip.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void particip_unpressed(){
        button_particip.setImage(new Image("/LoginView/particip_activated.jpg"));
    }

    /** button_stau_acasa ImageView*/
    public void stau_acasa_clicked() {
        CustomEventDTO eventDTO = events_table.getSelectionModel().getSelectedItem();
        service_eveniment.delParticipant(eventDTO.getId(), this.user.getId());
        events_list.setAll(getCustomEvents());
        events_list.forEach(System.out::println);
    }

    public void stau_acasa_hover(){
        button_stau_acasa.setImage(new Image("/LoginView/stau_acasa_activated.jpg"));
    }

    public void stau_acasa_unhover(){
        button_stau_acasa.setImage(new Image("/LoginView/button_stau_acasa.jpg"));
    }

    public void stau_acasa_pressed(){
        button_stau_acasa.setImage(new Image("/LoginView/button_pressed.jpg"));
    }

    public void stau_acasa_unpressed(){
        button_stau_acasa.setImage(new Image("/LoginView/stau_acasa_activated.jpg"));
    }



    /****************************************************************/
    /************** !KEYBOARD Binds! *******************/
    /****************************************************************/

    public void message_field_entered()
    {
        message_field.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                System.out.println("Message enter pressed");
                Vector<Long> rez = new Vector<>();

                rez.add(user2.getId());
                Mesaj msj = new Mesaj();
                msj.setSenderId(user.getId());
                msj.setReceiversId(rez.stream().collect(toList()));
                msj.setMessage(message_field.getText());
                message_field.setText("");

                service_mesaj.addUMesaj(msj);

                requests_table.setItems(friendshipList);
                friendshipList.removeAll();
                friendshipList.setAll(service_mesaj.conversationGetter(this.user.getId(),user2.getId()));
                friendshipList.forEach(System.out::println);
            }
        });

    }

    private void initUserModel() {
        pagination_pagination.currentPageIndexProperty().addListener((obs, oldIndex, newIndex) ->
                page_handler());
        Page<Utilizator> users = service_universal_user.getPagedUsers(pagination_pagination.getCurrentPageIndex());
        user_list.setAll(users.getContent().collect(Collectors.toList()));
    }

    public void page_handler() {
        initUserModel();
        //nume.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("firstName"));
        //prenume.setCellValueFactory(new PropertyValueFactory<Utilizator, String>("lastName"));
        pagination_table.setItems(user_list);
    }
}
