package socialnetwork.repository.database;

import socialnetwork.domain.Mesaj;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryDb;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class MesajeDbRepository implements RepositoryDb<Long, Mesaj> {
    private String url;
    private String username;
    private String password;
    private Validator<Mesaj> validator;

    public MesajeDbRepository(String url, String username, String password, Validator<Mesaj> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**Tested*/
    @Override
    public Iterable<Mesaj> findAll() {
        Set<Mesaj> messges = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from mesaje");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long senderId = resultSet.getLong("senderid");
                Long recieversId = resultSet.getLong("recieversid");
                String msg = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long responseTo = resultSet.getLong("responseto");
                Long id = resultSet.getLong("id");


                List<Long> list = new ArrayList<Long>();
                list.add(recieversId);

                Mesaj mesajNou = new Mesaj(senderId, list, msg, responseTo);
                mesajNou.setDate(date);
                mesajNou.setId(id);

                messges.add(mesajNou);
            }
            return messges;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messges;
    }


    /**Tested*/
    @Override
    public Optional<Mesaj> findOne(Long aLong){
        Optional<Mesaj> o = Optional.empty();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from mesaje WHERE mesaje.id = ?");
        )
        {
            statement.setLong(1, aLong);

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Long id = resultSet.getLong("id");
                Long senderId = resultSet.getLong("senderid");
                int nrRec = resultSet.getInt("nrrec");
                Long recieversId = resultSet.getLong("recieversid");
                Long responseTo = resultSet.getLong("responseto");
                String message = resultSet.getString("message");
                LocalDateTime data = resultSet.getTimestamp("date").toLocalDateTime();

                List<Long> list = new ArrayList<Long>();
                list.add(recieversId);

                Mesaj mesajNou = new Mesaj(senderId, list, message, responseTo);
                mesajNou.setDate(data);
                mesajNou.setId(id);

                o = Optional.of(mesajNou);}

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return o;
    }

    /**Tested*/
    @Override
    public Optional<Mesaj> save(Mesaj entity) {

        if(entity ==  null)
            throw new IllegalArgumentException("entity must not be null");
        this.validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO mesaje(senderid, nrrec, recieversid, message, date, responseto, id) VALUES (?,?,?,?,?,?,?)");
        ){
            statement.setLong(1, entity.getSenderId());
            statement.setInt(2, entity.getReceiversId().size());
            statement.setLong(3, entity.getReceiversId().get(0));
            statement.setString(4, entity.getMessage());
            statement.setTimestamp(5, Timestamp.valueOf(entity.getDate()));
            statement.setLong(6, entity.getResponseTo());
            statement.setLong(7, entity.getId());

            statement.executeUpdate();
            return Optional.empty();

        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return Optional.of(entity);
        }

    }

    /**Tested*/
    @Override
    public Optional<Mesaj> delete(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("id must not be null");
        Optional<Mesaj> o = findOne(aLong);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM mesaje WHERE id=? ");
        ){
            statement.setLong(1, aLong);
            statement.executeUpdate();

        }
        catch(SQLException e){
        }
        return o;
    }






    @Override
    public Optional<Mesaj> update(Mesaj entity) {
        return Optional.empty();
    }
}
