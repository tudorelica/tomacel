package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryDb;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class PrieteniiDbRepository implements RepositoryDb<Tuple<Long, Long>, Prietenie> {
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public PrieteniiDbRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**Tested*/
    @Override
    public Optional<Prietenie> findOne(Tuple <Long, Long> aLong){
        Optional<Prietenie> o = Optional.empty();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from prietenii WHERE prietenii.id1 = ? AND prietenii.id2 = ?");
        )
        {
            statement.setLong(1, aLong.getLeft());
            statement.setLong(2, aLong.getRight());

            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                LocalDateTime data = resultSet.getTimestamp("data").toLocalDateTime();
                int status = resultSet.getInt("status");

                Prietenie prietenie = new Prietenie();
                prietenie.setStatus(status);
                prietenie.setId(aLong);
                prietenie.setDate(data);


                o = Optional.of(prietenie);}

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return o;
    }

    /**Tested*/
    @Override
    public Iterable<Prietenie> findAll() {
        Set<Prietenie> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from prietenii");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                LocalDateTime data = resultSet.getTimestamp("data").toLocalDateTime();
                int status = resultSet.getInt("status");
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");


                Prietenie prietenie = new Prietenie();
                prietenie.setStatus(status);
                prietenie.setId(new Tuple<Long, Long>(id1, id2));
                prietenie.setDate(data);

                users.add(prietenie);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }


    /**Tested*/
    @Override
    public Optional<Prietenie> save(Prietenie entity){
        if(entity ==  null)
            throw new IllegalArgumentException("entity must not be null");
        this.validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO prietenii(id1, id2, data, status) VALUES (?,?,?,?)");
        ){
            statement.setLong(1, entity.getId().getLeft());
            statement.setTimestamp(3, Timestamp.valueOf(entity.getDate()));
            statement.setLong(2, entity.getId().getRight());
            statement.setInt(4, entity.getStatus());
            statement.executeUpdate();
            return Optional.empty();

        }
        catch(SQLException e){
            return Optional.of(entity);
        }
    }

    /**Tested*/
    @Override
    public Optional<Prietenie> delete(Tuple <Long, Long> aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("id must not be null");
        else
            if (findOne(aLong).isPresent())
            {
                Optional<Prietenie> o = findOne(aLong);
                try (Connection connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement = connection.prepareStatement("DELETE FROM prietenii WHERE id1=? AND id2=?");
                ){
                    statement.setLong(1, aLong.getLeft());
                    statement.setLong(2, aLong.getRight());
                    statement.executeUpdate();

                }
                catch(SQLException e){
                }
                return o;
            }
            else
                {
                    Optional<Prietenie> o = findOne(new Tuple <Long, Long>(aLong.getRight(), aLong.getLeft()));
                    try (Connection connection = DriverManager.getConnection(url, username, password);
                         PreparedStatement statement = connection.prepareStatement("DELETE FROM prietenii WHERE id1=? AND id2=?");
                    ){
                        statement.setLong(2, aLong.getLeft());
                        statement.setLong(1, aLong.getRight());
                        statement.executeUpdate();

                    }
                    catch(SQLException e){
                    }
                    return o;
                }

    }


    @Override
    public Optional<Prietenie> update(Prietenie entity){
        Prietenie prietenie = new Prietenie();
        prietenie.setStatus(0);
        prietenie.setId(new Tuple<Long, Long>(1l, 2l));
        prietenie.setDate(LocalDateTime.now());

        Optional<Prietenie> optional = Optional.of(prietenie);

        return optional;
    }
}
