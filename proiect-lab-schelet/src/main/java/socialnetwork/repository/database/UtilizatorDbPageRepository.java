package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

public class UtilizatorDbPageRepository extends UtilizatorDbRepository implements PagingRepository<Long, Utilizator> {
    String url;
    String username;
    String password;
    Validator<Utilizator> validator;
    public UtilizatorDbPageRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Page<Utilizator> findAll(Pageable pageable) {
        Paginator<Utilizator> utilizatorPaginator = new Paginator<>(pageable, findAll());
        return utilizatorPaginator.paginate();
    }
}
