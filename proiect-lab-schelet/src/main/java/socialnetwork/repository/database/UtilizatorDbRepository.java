package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryDb;

import java.sql.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class UtilizatorDbRepository implements RepositoryDb<Long, Utilizator> {
    private String url;
    private String username;
    private String password;
    private Validator<Utilizator> validator;

    public UtilizatorDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public Optional<Utilizator> findOne(Long aLong){
        Optional<Utilizator> o = Optional.empty();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from utilizatori WHERE utilizatori.id = ?");
        )
        {
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                String nume = resultSet.getString("nume");
                String prenume = resultSet.getString("prenume");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");

                Utilizator utilizator = new Utilizator(prenume, nume,username, password);
                utilizator.setId(aLong);


                o = Optional.of(utilizator);}

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return o;
    }


    @Override
    public Iterable<Utilizator> findAll() {
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from utilizatori");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String nume = resultSet.getString("nume");
                String prenume = resultSet.getString("prenume");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");

                Utilizator utilizator = new Utilizator(prenume, nume,username, password);
                utilizator.setId(id);

                users.add(utilizator);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Optional<Utilizator> save(Utilizator entity) {

        if(entity ==  null)
            throw new IllegalArgumentException("entity must not be null");
        this.validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO utilizatori(id, nume, prenume, username, password) VALUES (?,?,?,?,?)");
        ){
            statement.setLong(1, entity.getId());
            statement.setString(3, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(4, entity.getUsername());
            statement.setString(5, entity.getPassword());
            statement.executeUpdate();
            return Optional.empty();

        }
        catch(SQLException e){
            return Optional.of(entity);
        }

    }

    @Override
    public Optional<Utilizator> delete(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("id must not be null");
        Optional<Utilizator> o = findOne(aLong);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM utilizatori WHERE id=? ");
        ){
            statement.setLong(1, aLong);
            statement.executeUpdate();

        }
        catch(SQLException e){
        }
        return o;
    }

    @Override
    public Optional<Utilizator> update(Utilizator entity) {
        return Optional.empty();
    }
}
