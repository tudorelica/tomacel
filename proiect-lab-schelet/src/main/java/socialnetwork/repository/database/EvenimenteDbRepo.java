package socialnetwork.repository.database;


import socialnetwork.domain.CustomEvent;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryDb;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class EvenimenteDbRepo implements RepositoryDb<Long, CustomEvent> {
    private String url;
    private String username;
    private String password;
    private Validator<CustomEvent> validator;

    public EvenimenteDbRepo(String url, String username, String password, Validator<CustomEvent> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Optional<CustomEvent> findOne(Long aLong) {
        Optional<CustomEvent> o = Optional.empty();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from events WHERE events.id = ?");
        )
        {
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Long creatorId = resultSet.getLong("creatorId");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                String ids = resultSet.getString("participantsids");
                LocalDateTime targetDate = resultSet.getTimestamp("targetdate").toLocalDateTime();


                CustomEvent event = new CustomEvent(targetDate, creatorId, titlu,descriere);
                event.setId(aLong);
                event.setString(ids);

                o = Optional.of(event);}

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return o;
    }

    @Override
    public Iterable<CustomEvent> findAll() {
        Set<CustomEvent> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from events");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");

                Long creatorId = resultSet.getLong("creatorId");
                String titlu = resultSet.getString("titlu");
                String descriere = resultSet.getString("descriere");
                String ids = resultSet.getString("participantsids");
                LocalDateTime targetDate = resultSet.getTimestamp("targetdate").toLocalDateTime();


                CustomEvent event = new CustomEvent(targetDate, creatorId, titlu,descriere);
                event.setId(id);
                event.setString(ids);
                events.add(event);
            }
            return events;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Optional<CustomEvent> save(CustomEvent entity) {
        if(entity ==  null)
            throw new IllegalArgumentException("entity must not be null");
        this.validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO events(id, creatorid, titlu, descriere, participantsids, targetdate) VALUES (?,?,?,?,?,?)");
        ){
            statement.setLong(1, entity.getId());
            statement.setLong(2, entity.getCreatorId());
            statement.setString(3, entity.getTitlu());
            statement.setString(4, entity.getDescriere());
            statement.setString(5, entity.getStringIds());
            statement.setTimestamp(6, Timestamp.valueOf(entity.getTargetDate()));
            statement.executeUpdate();
            return Optional.empty();

        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return Optional.of(entity);
        }
    }

    @Override
    public Optional<CustomEvent> delete(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("id must not be null");
        Optional<CustomEvent> o = findOne(aLong);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM events WHERE id=? ");
        ){
            statement.setLong(1, aLong);
            statement.executeUpdate();

        }
        catch(SQLException e){
        }
        return o;
    }

    @Override
    public Optional<CustomEvent> update(CustomEvent entity) {
        delete(entity.getId());
        return save(entity);
    }
}
