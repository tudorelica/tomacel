package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.RepositoryException;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    private Validator<E> validator;
    Map<ID,E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities=new HashMap<ID,E>();
    }

    @Override
    public E findOne(ID id){
        if (id==null)
            throw new IllegalArgumentException("id must be not null");
        return entities.get(id);    //Da return la o entitate in functie de id
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    } //Un fel de getAll()

    @Override
    public E save(E entity) {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        try {
            validator.validate(entity);

            if (entities.get(entity.getId()) != null)
                return null;
            else
                entities.put(entity.getId(), entity);   //Asta e un fel de Add()
        }
        catch(ValidationException valEx)
        {
            System.out.println("<<<<<<" + valEx.getMessage() + ">>>>>>");
            throw new RepositoryException("Validation problem: " + valEx.getMessage());
        }
        return entity;
       //
    }

    /*
    @Override
    public E delete(ID id) {
        return null;
    }   //?
    */

    @Override
    public E delete(ID id) {
        if(findOne(id) == null)
            return null;
        else
        {
            E aux = entities.get(id);
            entities.remove(id);
            return aux;
        }
    }

    @Override
    public E update(E entity) {

        if(entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);

        entities.put(entity.getId(),entity);

        if(entities.get(entity.getId()) != null) {
            entities.put(entity.getId(),entity);
            return null;
        }
        return entity;

    }

}
