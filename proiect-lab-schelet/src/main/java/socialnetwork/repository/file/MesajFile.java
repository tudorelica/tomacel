package socialnetwork.repository.file;

import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;

public class MesajFile extends AbstractFileRepository<Long, Mesaj>{

    public MesajFile(String fileName, Validator<Mesaj> validator) {
        super(fileName, validator);
    }

    @Override
    public Mesaj extractEntity(List<String> attributes) {
        //TODO: implement method
        //Utilizator user = new Utilizator(attributes.get(1),attributes.get(2), attributes.get(3), attributes.get(4));
        //user.setId(Long.parseLong(attributes.get(0)));

        Long senderId = Long.parseLong(attributes.get(0));
        Long nrReceivers = Long.parseLong(attributes.get(1));

        Vector<Long> recieversVector = new Vector<Long>();

        for (Long index = 2L; index <= nrReceivers + 1L; index++ )
        {
            recieversVector.add(Long.parseLong(attributes.get(Math.toIntExact(index))));
        }
        List<Long> receivedIds = recieversVector.stream().collect(Collectors.toList());

        String msj = attributes.get(Math.toIntExact(nrReceivers + 1L + 1));

        LocalDateTime dateTime = LocalDateTime.parse(attributes.get(Math.toIntExact(nrReceivers + 1L + 2)));

        Long responseTo = Long.parseLong(attributes.get(Math.toIntExact(nrReceivers + 1L + 3)));
        Long realId = Long.parseLong(attributes.get(Math.toIntExact(nrReceivers + 1L + 4)));

        Mesaj mesajNou = new Mesaj(senderId, receivedIds, msj, responseTo);
        mesajNou.setDate(dateTime);
        mesajNou.setId(realId);

        return mesajNou;
    }

    @Override
    protected String createEntityAsString(Mesaj entity) {
        return entity.toString();
    }


}
