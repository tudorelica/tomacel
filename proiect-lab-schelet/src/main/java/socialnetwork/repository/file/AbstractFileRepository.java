package socialnetwork.repository.file;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepositoryException;
import socialnetwork.repository.memory.InMemoryRepository;

import java.io.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)
public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    String fileName;
    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);  // cheama constructoru din InMemoryRepository //Cand ai relatie de mostenire si clasa B extinde clasa A, clasa B trebuie sa apeleze construc clasei A prima data
        this.fileName=fileName;
        loadData();

    }

    private void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String linie;
            while((linie=br.readLine())!=null){
                List<String> attr=Arrays.asList(linie.split(";"));
                E e=extractEntity(attr);
                super.save(e);  //ca un fel de this pentru ala din care mostenesti (in cazu asta InMemoryRepository)
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //sau cu lambda - curs 4, sem 4 si 5
//        Path path = Paths.get(fileName);
//        try {
//            List<String> lines = Files.readAllLines(path);
//            lines.forEach(linie -> {
//                E entity=extractEntity(Arrays.asList(linie.split(";")));
//                super.save(entity);
//            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);
    ///Observatie-Sugestie: in locul metodei template extractEntity, puteti avea un factory pr crearea instantelor entity

    protected  String createEntityAsString(E entity)
    {
        return entity.toString();
    }

    @Override
    public E save(E entity){
        try {
            E e = super.save(entity);

            /*
            if (e == null) {
                writeToFile(entity);
                System.out.println("ajung aici");
            }*/
            writeToFile(entity);
            //System.out.println("nu ajung aici");
            return e;
        }
        catch (RepositoryException repEx)
        {
            System.out.println("<RepoExcep>" + repEx.getMessage() + "<RepoExcep>");
        }
        return null;

    }

    @Override
    public E delete(ID id) {
        E task = super.delete(id);

        try {
            PrintWriter writer = new PrintWriter(fileName);
            writer.print("");

            for (E entitate : findAll())
            {
                writeToFile(entitate);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Delete PrintWriter exception!");
            e.printStackTrace();
        }


        return task;
    }

    protected void writeToFile(E entity){
        try (BufferedWriter bW = new BufferedWriter(new FileWriter(fileName,true))) {
            bW.write(createEntityAsString(entity));
            bW.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}

