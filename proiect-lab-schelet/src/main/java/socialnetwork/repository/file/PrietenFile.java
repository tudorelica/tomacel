package socialnetwork.repository.file;

import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class PrietenFile extends AbstractFileRepository<Tuple<Long, Long>, Prietenie> {     //Id-u la prietenie e o cheie compusa

    public PrietenFile(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        //TODO: implement method
        Long one_id = Long.parseLong(attributes.get(0));
        Long another_id = Long.parseLong(attributes.get(1));
        System.out.println(attributes.get(0));
        System.out.println(attributes.get(1));
        System.out.println(attributes.get(2));
        System.out.println(attributes.get(3));


       // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-ddTHH:mm.sssssssss");
        //LocalDateTime actual_date = LocalDateTime.parse(attributes.get(2), formatter);

        List<String> attr= Arrays.asList(attributes.get(2).split("-"));
        List<String> attr2= Arrays.asList(attr.get(2).split("T"));
        List<String> attr3= Arrays.asList(attr2.get(1).split(":"));

        LocalDateTime dateTime = LocalDateTime.parse(attributes.get(2));

        /**LocalDateTime dateTime = LocalDateTime.of(Integer.parseInt(attr.get(0)),Integer.parseInt(attr.get(1))
                                                , Integer.parseInt(attr2.get(0)), Integer.parseInt(attr3.get(0)),
                                                Integer.parseInt(attr3.get(1)), Integer.parseInt(attr3.get(2)));
         */
        System.out.println("Date and time: " + dateTime);



        Prietenie prieteneala = new Prietenie(Integer.parseInt(attributes.get(3)));
        prieteneala.setId(new Tuple(one_id, another_id));
        //prieteneala.setStatus(Integer.parseInt(attr.get(3)));
        //prieteneala.setDate(actual_date);
        prieteneala.setDate(dateTime);
        return prieteneala;
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.toString();
    }
}
