package socialnetwork.repository.paging;


import socialnetwork.domain.Entity;
import socialnetwork.repository.RepositoryDb;

public interface PagingRepository<ID , E extends Entity<ID>> extends RepositoryDb<ID, E> {
    Page<E> findAll(Pageable pageable);
}

